import { BrandService } from '../brand.service';
import { Test } from '@nestjs/testing';
import { BrandEntity } from './support/brand.entity';
import { BrandsEntity } from '../entities/brands.entity';
import { brand } from '../proto/brand';
import { brandStub, exceptionStub } from './stubs/brand.stub';
import { BrandsByCategory } from '../entities/brands-by-category.entity';
import { BrandByCategoryEntityModel } from './support/brand-by-category';
import { ConfigModule } from '@nestjs/config';
import { SeaweedfsService } from '../../shared/seaweedfs.service';
import { brandEntityStub } from './stubs/brand.entity.stub';

describe('Brand Service', () => {
  let brandService: BrandService;
  let brandModel: BrandEntity;
  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [ConfigModule],
      providers: [
        BrandService,
        {
          provide: `${BrandsEntity.name}Model`,
          useClass: BrandEntity,
        },
        {
          provide: `${BrandsByCategory.name}Model`,
          useClass: BrandByCategoryEntityModel,
        },
        SeaweedfsService,
      ],
    }).compile();
    brandService = moduleRef.get<BrandService>(BrandService);
    brandModel = moduleRef.get<BrandEntity>(`${BrandsEntity.name}Model`);
    jest.clearAllMocks();
  });
  // describe('findBrand', () => {
  //   describe('when findBrand is called', () => {
  //     let findBrand: brand.FindBrandResponse;
  //     beforeEach(async () => {
  //       jest.spyOn(brandModel, 'search');
  //       findBrand = await brandService.findBrand({
  //         name: brandStub().name,
  //         pagination: {
  //           perPage: 8,
  //           currentPage: 1,
  //         },
  //       });
  //     });
  //     it('then it should call the brandModel', () => {
  //       expect(brandModel.search).resolves.toHaveBeenCalled();
  //     });
  //     it('then it should return Array of brand', () => {
  //       expect(findBrand).toEqual({
  //         brands: [brandStub()],
  //         exception: exceptionStub(),
  //       });
  //     });
  //   });
  // });
  describe('Test elassic result to brand Array', () => {
    let elasstic: brand.Brand[];

    describe('when elasstic is called', () => {
      beforeEach(async () => {
        elasstic = await brandService.elassticBrandsToBrand([
          brandEntityStub(),
        ]);
      });
    });
    it('then it should return Array of brand', () => {
      console.log(elasstic);
      expect(elasstic).toEqual([brandStub()]);
    });
  });
});
