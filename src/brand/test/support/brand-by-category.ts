import { BrandsEntity } from 'src/brand/entities/brands.entity';
import { MockModel } from '../../../database/test/support/mock.model';
import { brandEntityStub } from '../stubs/brand.entity.stub';

export class BrandByCategoryEntityModel extends MockModel<BrandsEntity> {
  protected entityStub = brandEntityStub();
}
