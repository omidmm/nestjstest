import { BrandController } from '../brand.controller';
import { BrandService } from '../brand.service';
import { Test } from '@nestjs/testing';
import { brand } from '../proto/brand';
import { brandStub, categoryIdStub, exceptionStub } from './stubs/brand.stub';
jest.mock('../brand.service');
describe('BrandController', () => {
  let brandController: BrandController;
  let brandService: BrandService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [],
      controllers: [BrandController],
      providers: [BrandService],
    }).compile();
    brandController = moduleRef.get<BrandController>(BrandController);
    brandService = moduleRef.get<BrandService>(BrandService);
    jest.clearAllMocks();
  });
  describe('findBrand', () => {
    describe('when findBrand is called', () => {
      let brandObj: brand.FindBrandResponse;
      beforeEach(async () => {
        brandObj = await brandController.findBrand({
          name: 'test',
          pagination: { currentPage: 1, perPage: 10 },
        });
      });
      test('then it should call brandService', () => {
        expect(brandService.findBrand).toHaveBeenCalled();
      });
      test('then is should return a Brand', () => {
        expect({
          brand: brandStub(),
          exception: exceptionStub(),
        }).toEqual({
          brand: brandStub(),
          exception: exceptionStub(),
        });
      });
    });
  });

  describe('updateFavBrand', () => {
    describe('when updateFavBrand is called', () => {
      let updateFavResponse: brand.UpdateFavBrandResponse;
      beforeEach(async () => {
        updateFavResponse = await brandController.updateFavBrand({
          brandId: brandStub().brandId,
          categoryId: categoryIdStub(),
        });
      });
      it('then it should call brandService', () => {
        expect(brandService.updateFavBrand).toHaveBeenCalled();
      });
      it('then it should return an exception  ', () => {
        expect(updateFavResponse).toEqual({ exception: exceptionStub() });
      });
    });
  });
  describe('getBrandOfCategory', () => {
    describe('when getBrandOfCategory is called', () => {
      let getBrandOfCategory: brand.GetBrandsOfCategoryResponse;
      beforeEach(async () => {
        getBrandOfCategory = await brandController.getBrandsOfCategory({
          categoryId: categoryIdStub(),
        });
      });
      it('then it should call brandService', () => {
        expect(brandService.getBrandsOfCategory).toHaveBeenCalledWith({
          categoryId: categoryIdStub(),
        });
      });
      it('then it should return brands', () => {
        expect(getBrandOfCategory).toEqual({
          brands: [brandStub()],
          pageState: 'pageState',
          exception: exceptionStub(),
        });
      });
    });
  });
});
