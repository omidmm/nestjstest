import { BrandsEntity } from '../../entities/brands.entity';
import { brandStub } from './brand.stub';

export const brandEntityStub = (): BrandsEntity => {
  return {
    name: brandStub().name,
    alias: brandStub().alias,
    description: brandStub().description,
    brand_id: brandStub().brandId,
    image: brandStub().image,
    created_at: new Date(),
    updated_at: new Date(),
    fav: 100,
  };
};
