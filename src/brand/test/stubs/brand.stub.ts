import { brand } from '../../proto/brand';
import { status } from 'grpc';
export const brandStub = (): brand.Brand => {
  return {
    alias: 'Brand Alias',
    brandId: '23424-324234-23423',
    description: 'this is description',
    image: 'image.png',
    name: 'Brand Name',
  };
};

export const categoryIdStub = (): string => {
  return 'category';
};

export const exceptionStub = (): brand.Exception => {
  return {
    statusCode: status.OK,
    message: 'Success',
  };
};
