import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { BrandService } from './brand.service';
import {
  Exception,
  CreateBrandInput,
  GetBrandsByCategoryResponse,
  SearchBrandsResponse,
  GetBrandsResponse,
  Pagination,
} from '../graphql';

@Resolver()
export class BrandResolver {
  constructor(private readonly brandService: BrandService) {}

  @Mutation()
  async createBrand(
    @Args('brands') brands: CreateBrandInput[],
  ): Promise<Exception> {
    return this.brandService.createBrand(brands);
  }

  @Mutation()
  async deleteBrand(@Args('brands') brands: string[]): Promise<Exception> {
    return this.brandService.deleteBrand(brands);
  }

  @Mutation()
  async removeBrandImage(@Args('brandId') brandId: string): Promise<Exception> {
    return this.brandService.removeBrandImage(brandId);
  }

  @Mutation()
  async updateBrand(
    @Args('brandId') brandId: string,
    @Args('brand') brand: CreateBrandInput,
  ): Promise<Exception> {
    return this.brandService.updateBrand(brandId, brand);
  }

  @Mutation()
  async addBrandToCategory(
    @Args('categoryId') categoryId: string,
    @Args('brands') brands: string[],
  ): Promise<Exception> {
    return this.brandService.addBrandToCategory(categoryId, brands);
  }

  @Mutation()
  async removeBrandfromCategory(
    @Args('categoryId') categoryId: string[],
    @Args('brands') brands: string[],
  ): Promise<Exception> {
    return this.brandService.removeBrandfromCategory(categoryId, brands);
  }

  @Query()
  async getBrands(
    @Args('pagination') pagination: Pagination,
  ): Promise<GetBrandsResponse> {
    return this.brandService.getBrands(pagination);
  }

  @Query()
  async getBrandsByCategory(
    @Args('categoryId') categoryId: string,
    @Args('pagination') pagination: Pagination,
  ): Promise<GetBrandsByCategoryResponse> {
    return this.brandService.getBrandsByCategory(categoryId, pagination);
  }

  @Query()
  async searchBrands(
    @Args('name') name: string,
    @Args('pagination') pagination: Pagination,
  ): Promise<SearchBrandsResponse> {
    return this.brandService.searchBrands(name, pagination);
  }
}
