import { Injectable } from '@nestjs/common';
import { InjectModel, BaseModel, uuid } from '@iaminfinity/express-cassandra';
import { BrandsEntity } from './entities/brands.entity';
import { BrandsByCategory } from './entities/brands-by-category.entity';
import { SeaweedfsService } from '../shared/seaweedfs.service';
import { brand } from './proto/brand';
import {
  Exception,
  CreateBrandInput,
  GetBrandsByCategoryResponse,
  SearchBrandsResponse,
  GetBrandsResponse,
  Brand,
  Pagination,
  BrandCategory,
} from '../graphql';
import * as _ from 'lodash';
import { status } from 'grpc';
@Injectable()
export class BrandService {
  constructor(
    @InjectModel(BrandsEntity) private brandsModel: BaseModel<BrandsEntity>,
    @InjectModel(BrandsByCategory)
    private brandsByCategoryModel: BaseModel<BrandsByCategory>,
    private readonly fileService: SeaweedfsService,
  ) {}

  /**
   * @description removing image of brand
   * @param brandId brandId
   * @returns exception
   */

  async removeBrandImage(brandId: string): Promise<Exception> {
    return this.brandsModel
      .findOneAsync({ brand_id: uuid(brandId) }, { allow_filtering: true })
      .then(brand => {
        if (brand !== undefined) {
          if (brand.image !== null) {
            return this.fileService.delete(brand.image).then(res => {
              if (res == true) {
                brand.image = null;

                this.brandsByCategoryModel
                  .findAsync(
                    { brand_id: uuid(brandId) },
                    { allow_filtering: true },
                  )
                  .then(brandcategories => {
                    if (brandcategories !== undefined) {
                      brandcategories.map(bc => {
                        bc.image = null;
                        bc.saveAsync();
                      });
                    }
                  });
                brand.saveAsync();
                return {
                  message: 'Item deleted',
                  statusCode: 200,
                };
              } else {
                return {
                  message: 'Item could not deleted',
                  statusCode: 201,
                };
              }
            });
          } else {
            return {
              message: 'sorry, image not set',
              statusCode: 404,
            };
          }
        } else {
          return {
            message: 'sorry, Item not found',
            statusCode: 404,
          };
        }
      });
  }
  async getBrandsByCategory(
    categoryId: string,
    pagination: Pagination,
  ): Promise<GetBrandsByCategoryResponse> {
    const response = await this.getBrandsOfCategory({
      categoryId: categoryId.toString(),
      pagination: { ...pagination },
    });
    const brands: BrandCategory[] = [];
    response.brands.map(brand => {
      brands.push({
        name: brand.name,
        categoryId: '',
        image: brand.image,
        brandId: brand.brandId,
      });
    });
    return {
      brands: brands,
      pageState: response.pageState,
      exception: {
        message: response.exception.message,
        statusCode: response.exception.statusCode,
      },
    };
  }
  async searchBrands(
    name: string,
    pagination: Pagination,
  ): Promise<SearchBrandsResponse> {
    const response = await this.findBrand({
      name: name.toString(),
      description: null,
      pagination: { ...pagination },
    });
    const brands: Brand[] = [];
    response.brands.map(brand => {
      brands.push({
        name: brand.name,
        brandId: brand.brandId,
        image: brand.image,
        description: brand.description,
      });
    });

    return {
      brands: brands,
      exception: {
        message: response.exception.message,
        statusCode: response.exception.statusCode,
      },
    };
  }
  async getBrands(pagination: Pagination): Promise<GetBrandsResponse> {
    // const subThis = this;
    const finalRes: GetBrandsResponse = {
      brands: [],
      pageState: null,
      exception: {
        message: '',
        statusCode: 404,
      },
    };
    return new Promise((resolve, reject) => {
      this.brandsModel.eachRow(
        {},
        { fetchSize: pagination.perPage, pageState: pagination.pageState },
        function(n, row) {
          finalRes.brands.push({
            name: row.name,
            description: row.description,
            brandId: row.brand_id,
            image: row.image,
          });
          resolve(finalRes);
        },
        function(err, result) {
          if (err) {
            console.log(err);
            return reject(err);
          } else {
            finalRes.exception.statusCode = 200;
            finalRes.pageState = result['pageState'];
            finalRes.exception.message = 'Success, Items founded';
            return resolve(finalRes);
          }
        },
      );
    });
  }
  /**
   * @description unlinking brands with category
   * @param categoryId
   * @param brands array of brands
   * @returns exception
   */
  async removeBrandfromCategory(
    categoryId: string[],
    brands: string[],
  ): Promise<Exception> {
    if (!_.isArray(brands)) {
      return {
        message: 'Please, Input array of brands',
        statusCode: 201,
      };
    }

    brands.forEach(brand =>
      this.brandsByCategoryModel.deleteAsync({
        brand_id: uuid(brand),
        category_id: uuid(categoryId),
      }),
    );

    return {
      message: 'Success, Items removed',
      statusCode: 200,
    };
  }

  /**
   * @description for linking brands to category
   * @param categoryId categoryId
   * @param brands list of brands names
   * @returns exception
   */
  async addBrandToCategory(
    categoryId: string,
    brands: string[],
  ): Promise<Exception> {
    if (!_.isArray(brands)) {
      return {
        message: 'Please, Input Array of Brand Id',
        statusCode: 201,
      };
    }
    for (let b = 0; b < brands.length; b++) {
      this.brandsModel
        .findOneAsync({ brand_id: uuid(brands[b]) }, { allow_filtering: true })
        .then(addBrand => {
          if (addBrand !== undefined) {
            new this.brandsByCategoryModel({
              name: addBrand.name,
              brand_id: uuid(addBrand.brand_id),
              category_id: uuid(categoryId),
              image: addBrand.image,
            }).saveAsync();
          }
        });
    }
    return {
      message: 'Success, Brands added to category',
      statusCode: 200,
    };
  }
  /**
   * @description update brand
   * @param brandId
   * @param brand
   * @returns
   */
  async updateBrand(
    brandId: string,
    brand: CreateBrandInput,
  ): Promise<Exception> {
    this.brandsModel
      .findOneAsync({ brand_id: uuid(brandId) }, { allow_filtering: true })
      .then(upBrand => {
        if (upBrand !== undefined) {
          upBrand.alias = [brand.alias];
          upBrand.description = [brand.description];
          if (brand.image.data !== undefined) {
            this.fileService.delete(upBrand.image).then(() => {
              this.fileService.writeFile(brand.image.data).then(wrRes => {
                if (_.isString(wrRes) && wrRes !== null) {
                  upBrand.image = wrRes;
                  this.brandsByCategoryModel
                    .findAsync(
                      { brand_id: uuid(brandId) },
                      { allow_filtering: true },
                    )
                    .then(items => {
                      items.map(bc => {
                        bc.image = wrRes;
                        bc.saveAsync();
                      });
                    });
                }
                upBrand.saveAsync();
              });
            });
          } else {
            upBrand.saveAsync();
          }
        }
      });
    return {
      message: 'success, Brand updated',
      statusCode: 200,
    };
  }
  async deleteBrand(brands: string[]): Promise<Exception> {
    if (!_.isArray(brands)) {
      return {
        message: 'Please Inter Array of brands',
        statusCode: 201,
      };
    }
    for (let b = 0; b < brands.length; b++) {
      this.brandsModel
        .findOneAsync({ brand_id: uuid(brands[b]) }, { allow_filtering: true })
        .then(brand => {
          if (brand !== undefined) {
            if (brand.image !== null) {
              this.fileService.delete(brand.image);
            }
            this.brandsByCategoryModel
              .findOneAsync(
                { brand_id: uuid(brand.brand_id) },
                { allow_filtering: true },
              )
              .then(bc => {
                if (bc !== undefined) {
                  bc.deleteAsync();
                }
              });

            brand.deleteAsync();
          }
        });
    }
    return {
      message: 'Success, item deleted',
      statusCode: 200,
    };
  }
  async createBrand(brands: CreateBrandInput[]): Promise<Exception> {
    if (!_.isArray(brands)) {
      return {
        message: 'brands not an Array',
        statusCode: 201,
      };
    }
    brands.forEach(async brand =>
      new this.brandsModel({
        name: brand.name,
        alias: brand.alias ? [brand.alias] : [brand.name],
        brand_id: uuid(),
        image:
          brand.image.data !== undefined
            ? await this.fileService.writeFile(brand.image.data)
            : null,
        description: [brand.description],
      }).saveAsync(),
    );

    return {
      message: 'success, brands added',
      statusCode: 200,
    };
  }
  /**
   * @description the method for add count to fav of brand ,later return brands sort by fav
   * @param data brandId, categoryid
   * @returns exception
   */
  async updateFavBrand(
    data: brand.UpdateFavBrandInput,
  ): Promise<brand.UpdateFavBrandResponse> {
    const brand = await this.brandsByCategoryModel.findOneAsync({
      category_id: uuid(data.categoryId),
      brand_id: uuid(data.brandId),
    });
    if (brand !== undefined) {
      this.brandsByCategoryModel
        .updateAsync(
          {
            category_id: uuid(brand.category_id),
            brand_id: uuid(brand.category_id),
            fav: brand.fav,
          },
          { fav: brand.fav === null ? 1 : brand.fav + 1 },
          { if_exists: true },
        )
        .then(() => {
          this.brandsModel
            .findOneAsync(
              { brand_id: uuid(data.brandId) },
              { allow_filtering: true },
            )
            .then(b => {
              if (b !== undefined) {
                b.fav = b.fav === null ? 1 : b.fav + 1;
                this.brandsModel.updateAsync(
                  { brand_id: uuid(data.brandId), fav: b.fav },
                  { fav: b.fav === null ? 1 : b.fav + 1 },
                  { if_exists: true },
                );
                //   b.saveAsync();
              }
            });
        });
      return {
        exception: {
          message: 'Success, Item updated',
          statusCode: status.OK,
        },
      };
    } else {
      return {
        exception: {
          message: 'item not found',
          statusCode: status.NOT_FOUND,
        },
      };
    }

    // return this.brandsByCategoryModel
    //   .findOneAsync({
    //     category_id: uuid(data.categoryId),
    //     brand_id: uuid(data.brandId),
    //   })
    //   .then(brand => {
    //     if (brand !== undefined) {
    //       this.brandsByCategoryModel
    //         .updateAsync(
    //           {
    //             category_id: uuid(brand.category_id),
    //             brand_id: uuid(brand.category_id),
    //             fav: brand.fav,
    //           },
    //           { fav: brand.fav === null ? 1 : brand.fav + 1 },
    //           { if_exists: true },
    //         )
    //         .then(res => {
    //           this.brandsModel
    //             .findOneAsync(
    //               { brand_id: uuid(data.brandId) },
    //               { allow_filtering: true },
    //             )
    //             .then(b => {
    //               if (b !== undefined) {
    //                 b.fav = b.fav === null ? 1 : b.fav + 1;
    //                 this.brandsModel.updateAsync(
    //                   { brand_id: uuid(data.brandId), fav: b.fav },
    //                   { fav: b.fav === null ? 1 : b.fav + 1 },
    //                   { if_exists: true },
    //                 );
    //                 //   b.saveAsync();
    //               }
    //             });
    //         });
    //       return {
    //         exception: {
    //           message: 'Success, Item updated',
    //           statusCode: status.OK,
    //         },
    //       };
    //     } else {
    //       return {
    //         exception: {
    //           message: 'item not found',
    //           statusCode: status.NOT_FOUND,
    //         },
    //       };
    //     }
    //   });
  }
  /**
   * @description finding brands
   * @param data {name, description, pagination}
   * @returns brands {brands array}, expcetion
   */
  async findBrand(
    data: brand.FindBrandInput,
  ): Promise<brand.FindBrandResponse> {
    const size = data.pagination.perPage > 1 ? data.pagination.perPage : 9;
    const from =
      data.pagination.currentPage > 1
        ? (data.pagination.currentPage - 1) * size
        : 0;
    const must = [];
    if (data.name !== undefined) {
      must.push({ match: { alias: data.name.toString() } });
    }
    if (data.description !== undefined) {
      if (data.description !== null)
        must.push({ match: { desciption: data.description } });
    }
    if (must.length === 0) {
      must.push({
        match_all: {},
      });
    }
    const body = {
      size,
      from,
      query: {
        bool: {
          must: must,
        },
      },
    };
    return new Promise((resolve, reject) => {
      this.brandsModel.search({ body: body }, (err, response) => {
        if (err) {
          return reject(err);
        }
        const finalRes: brand.FindBrandResponse = {
          brands: [],
          exception: {
            message: 'Success, Item founded',
            statusCode: status.OK,
          },
        };

        if (response.hits.hits.length > 0) {
          finalRes.brands = this.elassticBrandsToBrand(response.hits.hits);
        }
        return resolve(finalRes);
      });
    });
  }
  /**
   * @description maping result of eslasstic to Brand Array
   * @param hits  input from elasstic query
   * @returns array of brand
   */
  elassticBrandsToBrand(hits: Array<any>): brand.Brand[] {
    // const res: Array<brand.Brand> = [];
    // for (let h = 0; h < hits.length; h++) {
    //   res.push({
    //     name: hits[h]._source.name,
    //     brandId: hits[h]._source.brand_id,
    //     image: hits[h]._source.image !== null ? hits[h]._source.image : null,
    //   });
    // }
    // return res;

    return hits.map(hit => ({
      name: hit._source.name,
      brandId: hit._source.brand_id,
      image: hit._source.image,
    }));
  }
  async getBrandsOfCategory(
    data: brand.GetBrandsOfCategoryInput,
  ): Promise<brand.GetBrandsOfCategoryResponse> {
    const finalRes: brand.GetBrandsOfCategoryResponse = {
      pageState: null,
      brands: [],
      exception: {
        message: 'No init',
        statusCode: status.NOT_FOUND,
      },
    };
    return new Promise((resolve, reject) => {
      this.brandsByCategoryModel.eachRow(
        { category_id: uuid(data.categoryId) },
        {
          allow_filtering: true,
          pageState: data.pagination.pageState,
          fetchSize: data.pagination.perPage,
        },
        (n, row) => {
          finalRes.brands.push({
            name: row.name,
            image: row.image,
            brandId: row.brand_id,
          });
          resolve(finalRes);
        },

        (err, result) => {
          if (err) {
            console.log(err);
            return reject(err);
          } else {
            finalRes.exception.statusCode = status.OK;
            finalRes.pageState = result['pageState'];
            finalRes.exception.message = 'Success, Items founded';
            return resolve(finalRes);
          }
        },
      );
    });
    //   const brands = await this.brandsByCategoryModel.findAsync(
    //     { category_id: uuid(data.categoryId), $orderby: { $desc: 'fav' } },
    //     { fetchSize: data.pagination.perPage, allow_filtering: true,pageState: data.pagination.pageState },
    //   );

    //   if (!_.isArray(brands)) {
    //     return {
    //       exception: {
    //         message: 'Brands not found',
    //         statusCode: 404,
    //       },
    //     };
    //   }
    //   let res: brand.Brand[] = [];
    //   for (let b = 0; b < brands.length; b++) {
    //     res.push({
    //       name: brands[b].name,
    //       brandId: brands[b].brand_id,
    //       image:
    //         brands[b].image !== null
    //           ? await this.fileService.readFile(brands[b].image)
    //           : null,
    //     });
    //   }
    //   return {
    //     exception: {
    //       message: 'success, brands founded',
    //       statusCode: 200,
    //     },
    //   };
  }
}
