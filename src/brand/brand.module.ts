import { Module } from '@nestjs/common';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { SharedModule } from 'src/shared/shared.module';
import { BrandController } from './brand.controller';
import { BrandService } from './brand.service';
import { BrandResolver } from './brand.resolver';
import { BrandsEntity } from './entities/brands.entity';
import { BrandsByCategory } from './entities/brands-by-category.entity';

@Module({
  imports: [
    ExpressCassandraModule.forFeature([BrandsByCategory, BrandsEntity]),
    SharedModule,
  ],
  controllers: [BrandController],
  providers: [BrandService, BrandResolver],
})
export class BrandModule {}
