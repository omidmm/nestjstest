import { brandStub, exceptionStub } from '../test/stubs/brand.stub';
import * as grpc from 'grpc';

export const BrandService = jest.fn().mockReturnValue({
  findBrand: jest.fn().mockResolvedValue({
    brand: [brandStub()],
    exception: exceptionStub(),
  }),
  getBrandsOfCategory: jest.fn().mockReturnValue({
    brands: [brandStub()],
    pageState: 'pageState',
    exception: exceptionStub(),
  }),

  updateFavBrand: jest.fn().mockResolvedValue({
    exception: exceptionStub(),
  }),
});
