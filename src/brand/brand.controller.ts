import { Controller } from '@nestjs/common';
import { BrandService } from './brand.service';
import { brand } from './proto/brand';
import * as grpc from 'grpc';
import { GrpcMethod } from '@nestjs/microservices';

@Controller()
export class BrandController {
  constructor(private readonly brandService: BrandService) {}

  @GrpcMethod('BrandClient', 'GetBrandsOfCategory')
  async getBrandsOfCategory(
    data: brand.GetBrandsOfCategoryInput,
  ): Promise<brand.GetBrandsOfCategoryResponse> {
    return this.brandService.getBrandsOfCategory(data);
  }
  @GrpcMethod('BrandClient', 'FindBrand')
  async findBrand(
    data: brand.FindBrandInput,
  ): Promise<brand.FindBrandResponse> {
    return this.brandService.findBrand(data);
  }
  @GrpcMethod('BrandClient', 'UpdateFavBrand')
  async updateFavBrand(
    data: brand.UpdateFavBrandInput,
  ): Promise<brand.UpdateFavBrandResponse> {
    return this.brandService.updateFavBrand(data);
  }
}
