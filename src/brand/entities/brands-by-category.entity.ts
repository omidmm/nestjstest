import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'brands_by_category',
  key: [['category_id', 'brand_id'], 'fav'],
  clustering_order: { fav: 'desc' },

  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
    versions: {
      key: '__v1',
    },
  },
})
export class BrandsByCategory {
  @Column({
    type: 'uuid',
  })
  category_id: any;
  @Column({
    type: 'uuid',
  })
  brand_id: any;

  @Column({
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'text',
  })
  image: string;
  @Column({
    type: 'int',
    default: function() {
      return 1;
    },
  })
  fav: number;
  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
