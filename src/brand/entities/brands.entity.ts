import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity<BrandsEntity>({
  table_name: 'brands',

  key: [['brand_id', 'name'], 'fav'],
  clustering_order: { fav: 'desc' },
  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
    versions: {
      key: '__v1',
    },
  },
  es_index_mapping: {
    discover: '^(?!alias|descripion).*',
    properties: {
      alias: {
        type: 'text',
        index: 'true',
      },
      description: {
        type: 'text',
      },
    },
  },
})
export class BrandsEntity {
  @Column({
    type: 'uuid',
  })
  brand_id: any;
  @Column({
    type: 'varchar',
  })
  name: string;
  @Column({
    type: 'text',
  })
  image: string;

  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  alias: string;

  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  description: string;
  @Column({
    type: 'int',
    default: function() {
      return 1;
    },
  })
  fav: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
