import {
  Entity,
  Column,
  AfterDelete,
  InjectModel,
  BaseModel,
  GeneratedUUidColumn,
  BeforeSave,
  AfterSave,
  uuid,
} from '@iaminfinity/express-cassandra';
import { ProvinceByCountry } from './province-by-country.entity';
import { Injectable, Logger } from '@nestjs/common';

@Entity({
  table_name: 'countries',
  key: ['country_id'],
})
@Injectable()
export class CountriesEntity {
  @GeneratedUUidColumn()
  country_id: any;
  @Column({
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  name_locale: string;
  @Column({
    type: 'varchar',
  })
  continent: string;

  @Column({
    type: 'varchar',
  })
  lang: string;

  @Column({
    type: 'varchar',
  })
  currency: string;

  @Column({
    type: 'varchar',
  })
  description: string;
}
