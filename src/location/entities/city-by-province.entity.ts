import { Entity, Column } from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'city_by_province',
  key: ['province_id', 'city_id'],
})
export class CityByProvince {
  @Column({
    type: 'uuid',
  })
  province_id: any;

  @Column({
    type: 'uuid',
  })
  city_id: any;

  @Column({
    type: 'uuid',
  })
  country_id: any;
  @Column({
    type: 'varchar',
  })
  name: string;
  @Column({
    type: 'varchar',
  })
  name_locale: string;

  @Column({
    type: 'varchar',
  })
  province_name: string;
  @Column({
    type: 'varchar',
  })
  country_name: string;
}
