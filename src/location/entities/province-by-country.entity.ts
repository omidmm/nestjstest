import { Entity, Column } from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'province_by_country',
  key: ['country_id', 'province_id'],
})
export class ProvinceByCountry {
  @Column({
    type: 'uuid',
  })
  country_id: any;
  @Column({
    type: 'uuid',
  })
  province_id: any;
  @Column({
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'varchar',
  })
  name_locale: string;

  @Column({
    type: 'varchar',
  })
  country_name: string;

  @Column({
    type: 'text',
  })
  description: string;
}
