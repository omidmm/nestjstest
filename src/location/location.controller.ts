import { Controller } from '@nestjs/common';
import { LocationService } from './location.service';
import { GrpcMethod } from '@nestjs/microservices';
import { location } from './proto/location-grpc.interface';
import * as _ from 'lodash';
import { status } from 'grpc';
@Controller()
export class LocationController {
  constructor(private readonly locationService: LocationService) {}
  @GrpcMethod('LocationClient', 'FindAllCountries')
  async findAllCountries(): Promise<location.FindAllCountriesResponse> {
    return await this.locationService.findAllCountries();
  }

  @GrpcMethod('LocationClient', 'FindAllProvinces')
  async findAllProvinces(
    data: location.FindByCountryInput,
  ): Promise<location.FindAllProvincesRespone> {
    return {
      provinces: await this.locationService.findAllProvince(data.countryId),
      exception: {
        message: 'success',
        statusCode: status.OK,
      },
    };
  }

  @GrpcMethod('LocationClient', 'FindAllCities')
  async findAllCities(
    data: location.FindByProvinceInput,
  ): Promise<location.FindAllCitiesResponse> {
    return {
      cities: await this.locationService.findAllCity(data.provinceId),
      exception: {
        message: 'success',
        statusCode: status.OK,
      },
    };
  }

  @GrpcMethod('LocationClient', 'FindCountry')
  async findCountry(
    data: location.FindByCountryInput,
  ): Promise<location.FindCountryResponse> {
    return this.locationService.findCountry(data.countryId);
  }

  @GrpcMethod('LocationClient', 'FindProvince')
  async findProvince(
    data: location.FindByProvinceInput,
  ): Promise<location.FindProvinceResponse> {
    return this.locationService.findProvince(data.provinceId);
  }

  @GrpcMethod('LocationClient', 'FindCity')
  async findCity(
    data: location.FindByCityInput,
  ): Promise<location.FindCityResponse> {
    return this.locationService.findCity(data.cityId);
  }
}
