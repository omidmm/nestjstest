import { Module } from '@nestjs/common';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { SharedModule } from 'src/shared/shared.module';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { LocationResolver } from './location.resolver';
import { CityByProvince } from './entities/city-by-province.entity';
import { ProvinceByCountry } from './entities/province-by-country.entity';
import { CountriesEntity } from './entities/countries.entity';

@Module({
  imports: [
    ExpressCassandraModule.forFeature([
      CityByProvince,
      ProvinceByCountry,
      CountriesEntity,
    ]),
    SharedModule,
  ],
  controllers: [LocationController],
  providers: [LocationService, LocationResolver],
})
export class LocationModule {}
