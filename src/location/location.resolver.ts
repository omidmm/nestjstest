import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { LocationService } from './location.service';
import {
  CreateCountryInput,
  Country,
  Exception,
  Province,
  City,
  CreateCityInput,
  CreateProvinceInput,
  FindCountryResponse,
  FindAllProvinceResponse,
  FindAllCountriesResponse,
} from '../graphql';
import * as _ from 'lodash';

@Resolver('Location')
export class LocationResolver {
  constructor(private readonly locationService: LocationService) {}
  @Mutation()
  async addCountry(
    @Args('createCountryInput') createCountryInput: CreateCountryInput[],
  ): Promise<Exception> {
    if (!_.isArray(createCountryInput)) {
      return {
        message: 'Input not Array',
        statusCode: 201,
      };
    }
    return this.locationService.addCountry(createCountryInput);
  }
  @Mutation()
  async updateCountry(
    @Args('createCountryInput') createCountryInput: CreateCountryInput,
    @Args('countryId') countryId: string,
  ): Promise<Exception> {
    return this.locationService.updateCountry(createCountryInput, countryId);
  }
  @Mutation()
  async deleteCountry(
    @Args('countryId') countryId: string,
  ): Promise<Exception> {
    return this.locationService.deleteCountry(countryId);
  }

  @Mutation()
  async updateProvince(
    @Args('createProvinceInput') createProvinceInput: CreateProvinceInput,
    @Args('provinceId') provinceId: string,
  ): Promise<Exception> {
    return this.locationService.updateProvince(createProvinceInput, provinceId);
  }
  @Mutation()
  async deleteProvince(
    @Args('provinceId') provinceId: string,
    @Args('countryId') countryId: string,
  ): Promise<Exception> {
    return this.locationService.deleteProvince(provinceId, countryId);
  }
  @Mutation()
  async addProvince(
    @Args('createProvinceInput') createProvinceInput: CreateProvinceInput[],
  ): Promise<Exception> {
    if (!_.isArray(createProvinceInput)) {
      return {
        message: 'Input not Array',
        statusCode: 201,
      };
    }
    return this.locationService.addProvince(createProvinceInput);
  }
  @Mutation()
  async addCity(
    @Args('createCityInput') createCityInput: CreateCityInput[],
  ): Promise<Exception> {
    if (!_.isArray(createCityInput)) {
      return {
        message: 'Input not Array',
        statusCode: 201,
      };
    }
    return this.locationService.addCity(createCityInput);
  }

  @Mutation()
  async deleteCity(
    @Args('cityId') cityId: string,
    @Args('provinceId') provicneId: string,
  ): Promise<Exception> {
    return this.locationService.deleteCity(cityId, provicneId);
  }
  @Mutation()
  async updateCity(
    @Args('cityId') cityId: string,
    @Args('createCityInput') createCityInput: CreateCityInput,
  ): Promise<Exception> {
    return this.locationService.updateCity(cityId, createCityInput);
  }

  @Query()
  async findCountry(
    @Args('countryId') countryId: string,
  ): Promise<FindCountryResponse> {
    return await this.locationService.findCountry(countryId);
  }
  @Query()
  async findAllCountries(): Promise<FindAllCountriesResponse> {
    return this.locationService.findAllCountries();
  }
  @Query()
  async findAllProvince(
    @Args('countryId') countryId: string,
  ): Promise<FindAllProvinceResponse> {
    return {
      exception: {
        message: 'success',
        statusCode: 200,
      },
      provinces: await this.locationService.findAllProvince(countryId),
    };
  }
  @Query()
  async findAllCity(
    @Args('provinceId') provinceId: string,
  ): Promise<Array<City>> {
    return this.locationService.findAllCity(provinceId);
  }
}
