import { Injectable } from '@nestjs/common';
import { InjectModel, BaseModel, uuid } from '@iaminfinity/express-cassandra';
import { CountriesEntity } from './entities/countries.entity';
import { ProvinceByCountry } from './entities/province-by-country.entity';
import { CityByProvince } from './entities/city-by-province.entity';
import {
  Exception,
  City,
  Country,
  Province,
  CreateCountryInput,
  CreateCityInput,
  CreateProvinceInput,
  FindCountryResponse,
  FindAllCountriesResponse,
} from '../graphql';
import * as _ from 'lodash';

import { location } from './proto/location-grpc.interface';
import { isUUID, IsUUID } from 'class-validator';
import { status } from 'grpc';

@Injectable()
export class LocationService {
  async findAllCity(provinceId: string): Promise<Array<City>> {
    return this.cityByProvinceModel
      .findAsync({
        province_id: uuid(provinceId),
      })
      .then(cities => {
        const res: City[] = [];
        cities.map(city => {
          if (city !== undefined) {
            res.push({
              name: city.name,
              cityId: city.city_id,
              nameLocale: city.name_locale,
              provinceId: city.province_id,
              provinceName: city.province_name,
              countryId: city.country_id,
              countryName: city.country_name,
            });
          }
        });
        return res;
      });
  }
  async findAllProvince(countryId: string): Promise<Array<Province>> {
    return this.provinceByCountryModel
      .findAsync({
        country_id: uuid(countryId),
      })
      .then(provinces => {
        const response: Province[] = [];

        provinces.map(province => {
          if (province !== undefined) {
            response.push({
              name: province.name,
              nameLocale: province.name_locale,
              countryName: province.country_name,
              countryId: province.country_id,
              provinceId: province.province_id,
            });
          }
        });
        return response;
      });
  }
  async findAllCountries(): Promise<FindAllCountriesResponse> {
    return this.countriesModel.findAsync({}).then(countries => {
      const response: Country[] = [];
      countries.map(country => {
        if (country !== undefined) {
          response.push({
            name: country.name,
            nameLocale: country.name_locale,
            lang: country.lang,
            countryId: country.country_id,
            currency: country.currency,
            description: country.description,
          });
        }
      });
      return {
        countries: response,
        exception: {
          message: 'success',
          statusCode: status.OK,
        },
      };
    });
  }
  async findCountry(countryId: string): Promise<FindCountryResponse> {
    return this.countriesModel
      .findOneAsync({
        country_id: uuid(countryId),
      })
      .then(country => {
        if (country === undefined) {
          return {
            exception: {
              message: 'Item not found',
              statusCode: status.OK,
            },
          };
        }
        return {
          country: {
            name: country.name,
            countryId: country.country_id,
            nameLocale: country.name_locale,
            lang: country.lang,
            currency: country.currency,
          },
          exception: {
            message: 'Success',
            statusCode: status.OK,
          },
        };
      })
      .catch(err => {
        return {
          exception: {
            message: 'there is an Error: '.concat(_.toString(err)),
            statusCode: status.INTERNAL,
          },
        };
      });
  }

  async updateCity(
    cityId: string,
    createCityInput: CreateCityInput,
  ): Promise<Exception> {
    return this.cityByProvinceModel
      .findOneAsync(
        {
          city_id: uuid(cityId),
          province_id: uuid(createCityInput.provinceId),
        },
        { allow_filtering: true },
      )
      .then(city => {
        if (city === undefined) {
          return {
            message: 'Item not found',
            statusCode: 404,
          };
        }
        city.name = createCityInput.name;
        city.name_locale = createCityInput.nameLocale;
        city.province_id = uuid(createCityInput.provinceId);
        city.province_name = createCityInput.provinceName;
        city.country_id = uuid(createCityInput.countryId);
        city.country_name = createCityInput.countryName;
        if (city.saveAsync()) {
          return {
            message: 'success Item saved',
            statusCode: 200,
          };
        }
      })
      .catch(err => {
        return {
          message: 'Sorry, there is an Error! : '.concat(_.toString(err)),
          statusCode: 201,
        };
      });
  }
  async deleteCity(cityId: string, provinceId: string): Promise<Exception> {
    return this.cityByProvinceModel
      .deleteAsync({
        city_id: uuid(cityId),
        province_id: uuid(provinceId),
      })
      .then(res => {
        return {
          message: 'Success Item deleted',
          statusCode: 200,
        };
      })
      .catch(err => {
        return {
          message: 'there is an Error!'.concat(_.toString(err)),
          statusCode: 201,
        };
      });
  }

  async addCity(createCityInput: CreateCityInput[]): Promise<Exception> {
    const notAccepted = [];
    for (let index = 0; index < createCityInput.length; index++) {
      const province = await this.provinceByCountryModel.findOneAsync(
        {
          province_id: uuid(createCityInput[index].provinceId),
        },
        { allow_filtering: true },
      );
      if (province !== undefined) {
        const newCity = new this.cityByProvinceModel({
          city_id: uuid(),
          name: createCityInput[index].name,
          name_locale: createCityInput[index].nameLocale,
          province_id: uuid(createCityInput[index].provinceId),
          province_name: createCityInput[index].provinceName,
          country_id: uuid(createCityInput[index].countryId),
          country_name: createCityInput[index].countryName,
        });
        newCity.saveAsync({ if_not_exist: true });
      } else {
        notAccepted.push({ province_id: createCityInput[index].provinceId });
      }
    }
    return {
      message: 'Success, Item added ,if not accepted '.concat(
        _.map(notAccepted, 'province_id').join(', '),
      ),
      statusCode: 200,
    };
    // const province = await this.provinceByCountryModel.findOneAsync(
    //   {
    //     province_id: uuid(createCityInput.provinceId),
    //   },
    //   { allow_filtering: true },
    // );
    // if (province === undefined) {
    //   return {
    //     message: 'province not found',
    //     statusCode: 404,
    //   };
    // }
    // const newCity = new this.cityByProvinceModel({
    //   city_id: uuid(),
    //   name: createCityInput.name,
    //   name_locale: createCityInput.nameLocale,
    //   province_id: uuid(createCityInput.provinceId),
    //   province_name: createCityInput.provinceName,
    //   country_id: uuid(createCityInput.countryId),
    //   country_name: createCityInput.countryName,
    // });
    // if (newCity.saveAsync()) {
    //   return {
    //     message: 'success, Item saved',
    //     statusCode: 200,
    //   };
    // } else {
    //   return {
    //     message: 'Sorry , Item can not saved',
    //     statusCode: 201,
    //   };
    // }
  }
  async deleteProvince(provinceId: string, countryId): Promise<Exception> {
    return this.provinceByCountryModel
      .deleteAsync({
        province_id: uuid(provinceId),
        country_id: uuid(countryId),
      })
      .then(res => {
        return this.deleteCityByProvince(provinceId)
          .then(res => {
            return {
              message: 'success Item deleted',
              statusCode: 200,
            };
          })
          .catch(err => {
            return {
              message: 'Sorry , there is an Error!'.concat(_.toString(err)),
              statusCode: 201,
            };
          });
      })
      .catch(err => {
        return {
          message: 'Sorry , there is an Error!'.concat(_.toString(err)),
          statusCode: 201,
        };
      });
  }
  async updateProvince(
    createProvinceInput: CreateProvinceInput,
    provinceId: string,
  ): Promise<Exception> {
    return this.provinceByCountryModel
      .findOneAsync(
        {
          province_id: uuid(provinceId),
        },
        { allow_filtering: true },
      )
      .then(province => {
        if (province == undefined) {
          return {
            message: 'Item not found',
            statusCode: 404,
          };
        }

        province.name = createProvinceInput.name;
        province.name_locale = createProvinceInput.nameLocale;
        province.country_id = uuid(createProvinceInput.countryId);
        province.country_name = createProvinceInput.countryName;
        return province
          .saveAsync()
          .then(res => {
            return {
              message: 'Success, Item saved',
              statusCode: 200,
            };
          })
          .catch(err => {
            return {
              message: 'Sorry, Item cannot updated, Error!:'.concat(err),
              statusCode: 201,
            };
          });
      });
  }
  async addProvince(
    createProvinceInput: CreateProvinceInput[],
  ): Promise<Exception> {
    const notAccepted = [];
    for (let index = 0; index < createProvinceInput.length; index++) {
      if (
        (await this.countriesModel.findOneAsync({
          country_id: uuid(createProvinceInput[index].countryId),
        })) !== undefined
      ) {
        const newProvince = new this.provinceByCountryModel({
          province_id: uuid(),
          name: createProvinceInput[index].name,
          name_locale: createProvinceInput[index].nameLocale,
          country_id: uuid(createProvinceInput[index].countryId),
          country_name: createProvinceInput[index].countryName,
        });
        newProvince.saveAsync();
      } else {
        notAccepted.push({ country_id: createProvinceInput[index].countryId });
      }
    }
    return {
      message: 'Success, Items added, if not Accepted ='.concat(
        _.map(notAccepted, 'country_id').join(', '),
      ),
      statusCode: 200,
    };
    // if (
    //   (await this.countriesModel.findOneAsync({
    //     country_id: uuid(createProvinceInput.countryId),
    //   })) === undefined
    // ) {
    //   return {
    //     message: 'country not found',
    //     statusCode: 404,
    //   };
    // }
    // const newProvince = new this.provinceByCountryModel({
    //   province_id: uuid(),
    //   name: createProvinceInput.name,
    //   name_locale: createProvinceInput.nameLocale,
    //   country_id: uuid(createProvinceInput.countryId),
    //   country_name: createProvinceInput.name,
    // });
    // if (await newProvince.saveAsync()) {
    //   return {
    //     message: 'Success Item created',
    //     statusCode: 200,
    //   };
    // } else {
    //   return {
    //     message: 'Sorry Item cannot added',
    //     statusCode: 201,
    //   };
    // }
  }

  async deleteProvinceByCountry(countryId: string): Promise<any> {
    const provinces = await this.provinceByCountryModel.findAsync({
      country_id: uuid(countryId),
    });
    provinces.forEach(province => {
      province.deleteAsync();
      this.deleteCityByProvince(province.province_id);
    }, this);
  }

  async deleteCityByProvince(provinceId: string) {
    const cities = await this.cityByProvinceModel.findAsync({
      province_id: uuid(provinceId),
    });
    cities.forEach(city => {
      city.deleteAsync();
    });
  }
  async deleteCountry(countryId: string): Promise<Exception> {
    this.countriesModel.deleteAsync({ country_id: uuid(countryId) });
    this.deleteProvinceByCountry(countryId);
    return {
      message: 'success',
      statusCode: 200,
    };
  }
  async updateCountry(
    createCountryInput: CreateCountryInput,
    countryId: string,
  ): Promise<Exception> {
    const country = await this.countriesModel.findOneAsync({
      country_id: uuid(countryId),
    });
    if (country === undefined) {
      return {
        message: 'Item not found',
        statusCode: 404,
      };
    }
    country.name = createCountryInput.name;
    country.name_locale = createCountryInput.nameLocale;
    country.lang = createCountryInput.lang;
    country.description = createCountryInput.description;
    country.continent = createCountryInput.continent;
    country.currency = createCountryInput.currency;

    if (country.saveAsync()) {
      return {
        message: 'success Item updated',
        statusCode: 200,
      };
    } else {
      return {
        message: 'Sorry, Item cannot updated',
        statusCode: 201,
      };
    }
  }
  async addCountry(
    createCountryInput: CreateCountryInput[],
  ): Promise<Exception> {
    createCountryInput.forEach(createCountryInput => {
      const newCountry = new this.countriesModel({
        name: createCountryInput.name,
        name_locale: createCountryInput.nameLocale,
        lang: createCountryInput.lang,
        currency: createCountryInput.currency,
        continent: createCountryInput.continent,
        description: createCountryInput.description,
      });
      newCountry.saveAsync();
    });

    return {
      message: 'Success item created',
      statusCode: 200,
    };
  }
  async findCity(cityId: string): Promise<location.FindCityResponse> {
    const city = await this.cityByProvinceModel.findOneAsync(
      {
        city_id: uuid(cityId),
      },
      { allow_filtering: true },
    );
    if (city === undefined) {
      return {
        exception: {
          message: 'Item not found',
          statusCode: 404,
        },
      };
    }
    return {
      exception: {
        message: 'success item founded',
        statusCode: 200,
      },
      city: {
        name: city.name,
        nameLocale: city.name_locale,
        cityId: city.city_id,
        provinceId: city.province_id,
        provinceName: city.province_name,
        countryId: city.country_id,
        countryName: city.country_name,
      },
    };
  }

  async findProvince(
    provinceId: string,
  ): Promise<location.FindProvinceResponse> {
    const province = await this.provinceByCountryModel.findOneAsync(
      {
        province_id: uuid(provinceId),
      },
      { allow_filtering: true },
    );
    if (province === undefined) {
      return {
        exception: {
          message: 'Sorry, Item not found',
          statusCode: 404,
        },
      };
    }
    return {
      province: {
        name: province.name,
        nameLocale: province.name_locale,
        countryId: province.country_id,
        countryName: province.country_name,
      },
      exception: {
        message: 'Success, item founded',
        statusCode: 200,
      },
    };
  }

  constructor(
    @InjectModel(CountriesEntity)
    private readonly countriesModel: BaseModel<CountriesEntity>,
    @InjectModel(ProvinceByCountry)
    private readonly provinceByCountryModel: BaseModel<ProvinceByCountry>,
    @InjectModel(CityByProvince)
    private readonly cityByProvinceModel: BaseModel<CityByProvince>,
  ) {}
}
