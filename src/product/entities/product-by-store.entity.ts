import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity<ProductByStoreEntity>({
  table_name: 'product_by_store',
  key: [['store_id', 'product_id']],
  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
  es_index_mapping: {
    discover: '^(?!location|name|description|short_description|variants).*',
    // discover: '.*',

    properties: {
      name: {
        type: 'text',
        index: 'true',
      },
      variants: {
        type: 'object',
      },
      address: {
        type: 'object',
      },

      description: {
        type: 'text',
        index: 'true',
      },
      short_description: {
        type: 'text',
        index: 'true',
      },
      categories: {
        type: 'object',
      },
      review_rating: {
        type: 'long',
      },
      review_count: {
        type: 'long',
      },

      location: {
        type: 'geo_point',
      },
    },
  },
})
export class ProductByStoreEntity {
  @Column({
    type: 'varchar',
  })
  store_id: string;

  @Column({
    type: 'timeuuid',
  })
  product_id: any;
  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  name: string[];

  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  store_name: string[];

  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  short_description: string[];
  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  description: string[];

  @Column({
    type: 'float',
  })
  weight: number;

  @Column({
    type: 'int',
  })
  quantity_default: number;
  @Column({
    type: 'int',
  })
  quantity_min: number;
  @Column({
    type: 'int',
  })
  quantity_max: number;
  @Column({
    type: 'int',
  })
  stock: number;
  @Column({
    type: 'boolean',
  })
  stackable: boolean;

  @Column({
    type: 'boolean',
  })
  shippable: boolean;

  @Column({
    type: 'boolean',
    default: () => {
      return false;
    },
  })
  published: boolean;

  @Column({
    type: 'varchar',
  })
  mpn: string;

  @Column({
    type: 'varchar',
  })
  gtin: string;
  @Column({
    type: 'int',
  })
  sale_count: number;
  @Column({
    type: 'map',
    typeDef: '<varchar,decimal>',
  })
  price: object;

  @Column({
    type: 'map',
    typeDef: '<varchar,decimal>',
  })
  on_sale_price: object;
  @Column({
    type: 'frozen',
    typeDef: '<map<int,frozen<map<varchar,decimal>>>>',
  })
  whole_sale: object;
  @Column({
    type: 'varchar',
  })
  primary_image: string;
  @Column({
    type: 'boolean',
  })
  allow_out_of_stock: boolean;
  @Column({
    type: 'varchar',
  })
  lang: string;
  @Column({ type: 'set', typeDef: '<text>' })
  media: string[];
  @Column({
    type: 'set',
    typeDef: '<varchar>',
    default: function() {
      return [];
    },
  })
  accessory: string[];
  @Column({
    type: 'uuid',
  })
  brand_id: any;

  @Column({
    type: 'list',
    typeDef: '<text>',
  })
  brand_name: string[];

  @Column({
    type: 'frozen',
    typeDef: '<address>',
  })
  address: any;

  @Column({
    type: 'list',
    typeDef: '<frozen<geo_point>>',
  })
  location: object;
  @Column({
    type: 'set',
    typeDef: '<frozen<category>>',
  })
  categories: object;

  @Column({
    type: 'map',
    typeDef: '<varchar,frozen<variant>>',
  })
  variants: object;

  @Column({
    type: 'map',
    typeDef: '<varchar,boolean>',
  })
  custom_field: any;

  @Column({
    type: 'frozen',
    typeDef: '<translation>',
  })
  translation: any;

  @Column({
    type: 'float',
  })
  review_rating: number;

  @Column({
    type: 'bigint',
  })
  review_count: number;

  @Column({
    type: 'timeuuid',
  })
  created_by: any;
  @Column({
    type: 'timeuuid',
  })
  updated_by: any;
  @CreateDateColumn()
  created_at: Date;
  @UpdateDateColumn()
  updated_at: Date;
}
