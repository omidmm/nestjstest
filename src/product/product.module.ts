import { Module } from '@nestjs/common';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { SharedModule } from 'src/shared/shared.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { ProductByStoreEntity } from './entities/product-by-store.entity';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ExpressCassandraModule.forFeature([ProductByStoreEntity]),
    SharedModule,

    ClientsModule.registerAsync([
      {
        name: 'STORE_PACKAGE',
        inject: [ConfigService],
        imports: [ConfigModule],
        useFactory: (configService: ConfigService) => ({
          transport: Transport.GRPC,
          options: {
            url: configService.get<string>('GRPC_STORE_URL'),
            package: 'store',
            protoPath: join(process.cwd(), 'src/store/proto/store.proto'),
          },
        }),
      },
    ]),
  ],
  controllers: [ProductController],
  providers: [ProductService],
})
export class ProductModule {}
