export namespace chunk {
  export interface ChunkInfo {
    fid: string;
    size: number;
    offset: number;
  }
  export interface ChunkList {
    name?: string;
    mime?: string;
    size?: number;
    chunks: ChunkInfo[];
  }
}
