import { Injectable, OnModuleInit, Inject } from '@nestjs/common';
import { Metadata } from 'grpc';
import { product } from './proto/product';
import {
  InjectModel,
  BaseModel,
  timeuuid,
  uuid,
} from '@iaminfinity/express-cassandra';
import { ProductByStoreEntity } from './entities/product-by-store.entity';
import { SeaweedfsService } from 'src/shared/seaweedfs.service';
import * as _ from 'lodash';
import { store } from 'src/store/proto/store';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable, Subject, throwError, TimeoutError } from 'rxjs';
import { timeout, catchError, first } from 'rxjs/operators';
import { chunk } from './class/chunk-file.interface';
import { fs } from 'memfs';
import { status } from 'grpc';

@Injectable()
export class ProductService implements OnModuleInit {
  addMediaStream(data: Observable<any>, metadata: any): Observable<any> {
    let progress = 0;
    const complete = false;
    let size = 0;
    console.log(metadata);
    const user = metadata.user !== undefined ? metadata.user : null;
    const storeId =
      metadata._internal_repr.storeid !== undefined
        ? metadata._internal_repr.storeid[0]
        : null;
    const productId =
      metadata._internal_repr.productid !== undefined
        ? metadata._internal_repr.productid[0]
        : null;
    const variantId =
      metadata._internal_repr.variantid !== undefined
        ? metadata._internal_repr.variantid[0]
        : undefined;
    const chunkSize =
      metadata._internal_repr.chunksize !== undefined
        ? parseInt(metadata._internal_repr.chunksize[0])
        : null;
    const fileSize =
      metadata._internal_repr.filesize !== undefined
        ? parseInt(metadata._internal_repr.filesize[0])
        : null;
    const mime =
      metadata._internal_repr.mime !== undefined
        ? metadata._internal_repr.mime[0]
        : null;
    const fileName =
      metadata._internal_repr.filename !== undefined
        ? metadata._internal_repr.filename[0]
        : null;
    console.log(fileSize, chunkSize);
    const chunklist: chunk.ChunkList = {
      chunks: [],
      size: fileSize,
      name: fileName,
      mime: mime,
    };
    const promiseArray: Array<Promise<any>> = [];

    const subject = new Subject();
    const checkStaff = this.storeClient.checkStaff({
      storeId: storeId,
      userId: user.userId,
      permission: this.permission,
    });

    if (productId === null) {
      const onNext = message => {
        subject.next({
          progress: progress++,

          exception: {
            message: 'you can not access to the store',
            statusCode: status.INTERNAL,
          },
        });
        subject.complete();
        const onComplete = () => {
          subject.complete();
        };
        data.subscribe(onNext, null, onComplete);
        return subject.asObservable();
      };
    } else {
      const onNext = message => {
        promiseArray.push(
          this.fileService.writeChunk(message.data).then(res => {
            console.log(res.data.fid);
            size = size + res.data.size;
            console.log(message.chunkId);
            chunklist.chunks.push({
              fid: res.data.fid,
              size: res.data.size,
              offset: (message.chunkId - 1) * chunkSize + res.data.size,
            });
          }),
        );
        subject.next({
          exception: {
            message: 'uploading',
            statusCode: status.OK,
          },
          progress: ((chunkSize * message.chunkId) / fileSize) * 100,
          chunkId: message.chunkId,
          chunkSize: message.chunkSize,
        });
      };

      const onError = () => {
        chunklist.chunks.forEach(chunk => {
          this.fileService.delete(chunk.fid);
        });
        subject.next({
          exception: {
            message: 'There is an error: on uploading file ',
            statusCode: status.INTERNAL,
          },
          progress: 1,
        });
      };

      const onComplete = () => {
        Promise.all(promiseArray)
          .then(response => {
            chunklist.size = size;
            const buf = Buffer.from(JSON.stringify(chunklist));
            this.addMedia(
              {
                productId: productId,
                variantId: variantId,
                storeId: storeId,
                media: { data: buf },
              },
              null,
              user,
              ['cm=true'],
            )
              .then(res => {
                subject.next({
                  exception: {
                    message: res.exception.message,
                    statusCode: res.exception.statusCode,
                  },
                  progress: 100,
                });
                console.log(res);
                if (res.exception.statusCode === status.OK) {
                  subject.complete();
                } else {
                  onError();
                }
              })
              .catch(err => {
                onError();
              });
          })
          .catch(err => {
            onError();
          });
      };
      checkStaff.pipe(first()).subscribe(x => {
        if (x.exception.statusCode === status.OK) {
          data.pipe(timeout(10000)).subscribe(onNext, onError, onComplete);
        } else {
          data.pipe(timeout(10000)).subscribe(
            x => {
              {
                subject.next({
                  progress: progress++,

                  exception: {
                    message: 'you can not access to the store',
                    statusCode: status.INTERNAL,
                  },
                });
                subject.complete();
              }
            },
            onError,
            null,
          );
        }
      });
      return subject.asObservable();
    }

    return subject.asObservable();
  }

  private permission = 'ManageProduct';
  private storeClient: store.StoreClient;
  onModuleInit() {
    this.storeClient = this.stClient.getService<store.StoreClient>(
      'StoreClient',
    );
  }
  constructor(
    @InjectModel(ProductByStoreEntity)
    private readonly productByStoreModel: BaseModel<ProductByStoreEntity>,
    private readonly fileService: SeaweedfsService,
    @Inject('STORE_PACKAGE') private stClient: ClientGrpc,
  ) {}
  private async checkStaff(
    storeId: string,
    userId: string,
    role: string = this.permission,
  ): Promise<product.Exception> {
    const checkStaff = await this.storeClient
      .checkStaff({
        storeId: storeId,
        userId: userId,
        permission: role,
      })
      .toPromise();
    if (checkStaff === undefined) {
      return {
        message: 'Sorry, connection broken!',
        statusCode: status.NOT_FOUND,
      };
    }
    return {
      message: checkStaff.exception.message,
      statusCode: checkStaff.exception.statusCode,
    };
  }
  async retriveProducts(
    data: product.RetriveProductsInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.RetriveProductsResponse> {
    const finalRes: product.RetriveProductsResponse = {
      pageState: null,
      products: [],
      exception: {
        message: 'not init',
        statusCode: status.NOT_FOUND,
      },
    };
    return new Promise((resolve, reject) => {
      this.productByStoreModel.eachRow(
        { store_id: data.storeId },
        {
          allow_filtering: true,
          pageState: data.pagination.pageState,
          fetchSize: data.pagination.perPage,
        },
        (n, product) => {
          finalRes.products.push(ProductService.ProductOutput(product));
        },
        (err, result) => {
          if (err) {
            (finalRes.exception.message = 'Sorry, there is an Error! : '.concat(
              _.toString(err),
            )),
              reject(err);
          } else {
            finalRes.exception.statusCode = status.OK;
            finalRes.exception.message = 'Success, products founded';
            finalRes.pageState = result['pageState'];

            return resolve(finalRes);
          }
        },
      );
    });
  }
  async findProducts(
    data: product.FindProductsInput,
    metadata: Metadata,
  ): Promise<product.FindProductsResponse> {
    const must = [];
    const bool = { must: {} };
    let body = {};
    const address = {
      nested: {
        path: 'address',
        query: {
          bool: {
            must: [],
          },
        },
      },
    };

    if (data.name !== undefined) {
      must.push({ match: { name: data.name } });
    }
    if (data.mpn !== undefined) {
      must.push({ match: { mpn: data.mpn } });
    }

    if (data.gtin !== undefined) {
      must.push({ match: { gtin: data.gtin } });
    }
    if (data.storeId !== undefined) {
      must.push({ match: { store_id: data.storeId } });
    }

    if (data.storeName !== undefined) {
      must.push({ match: { store_name: data.storeName } });
    }
    if (data.categoryIds !== undefined) {
      const categories = {
        nested: {
          path: 'categories',
          query: {
            bool: {
              must: [],
            },
          },
        },
      };
      data.categoryIds.forEach(catId => {
        categories.nested.query.bool.must.push({
          term: {
            'categories.category_id': catId,
          },
        });
      });
      if (categories.nested.query.bool.must.length > 0) {
        must.push({ nested: categories.nested });
      }
    }
    if (data.filterFind !== undefined) {
    }
    if (data.description !== undefined) {
      must.push({ match: { description: data.description } });
    }
    if (data.cityId !== undefined) {
      address.nested.query.bool.must.push({
        term: {
          'address.city_id': data.cityId,
        },
      });
    }
    if (data.countryId !== undefined) {
      address.nested.query.bool.must.push({
        term: {
          'address.country_id': data.countryId,
        },
      });
    }
    if (data.provinceId !== undefined) {
      address.nested.query.bool.must.push({
        term: {
          'address.province_id': data.provinceId,
        },
      });
    }
    if (address.nested.query.bool.must.length > 0) {
      must.push({ nested: address.nested });
    }
    if (must.length == 0) {
      must.push({
        match_all: {},
      });
    }
    bool.must = must;

    if (
      data.nearMe > 0 &&
      (data.distanceInd === 'km' || data.distanceInd === 'm')
    ) {
      _.assign(bool, {
        filter: {
          geo_distance: {
            distance: `${data.nearMe}${data.distanceInd}`,
            location: { ...data.geoPoint },
          },
        },
      });
    }
    body = {
      size:
        data.pagination.perPage !== undefined
          ? data.pagination.perPage > 1
            ? data.pagination.perPage
            : 9
          : 9,
      from:
        data.pagination.currentPage !== undefined
          ? data.pagination.currentPage > 1
            ? (data.pagination.currentPage - 1) *
              (data.pagination.perPage > 1 ? data.pagination.perPage : 9)
            : 0
          : 0,
      query: {
        bool: bool,
      },
    };
    return new Promise((resolve, reject) => {
      this.productByStoreModel.search(
        {
          body: body,
        },
        (err, response) => {
          if (err) {
            return reject(err);
          } else {
            const finalRes: product.FindProductsResponse = {
              products: [],
              exception: { message: 'success', statusCode: status.OK },
            };
            if (response.hits.hits.length > 0) {
              finalRes.products = ProductService.SearchProductOutput(
                response.hits.hits,
              );
            }

            return resolve(finalRes);
          }
        },
      );
    });
  }
  static SearchProductOutput(hits: Array<any>): product.Product[] {
    const res: product.Product[] = [];

    hits.forEach(product => {
      const categories: product.Category[] = [];

      const price: product.Price[] = [];

      _.each(product._source.price, function(value, key) {
        price.push({
          currencyCode: key,
          amount: value,
        });
      });

      if (typeof product._source.categories.length === 'undefined') {
        categories.push({
          categoryId: product._source.categories.category_id,
          categoryName: product._source.categories.category_name,
        });
      } else {
        _.each(product._source.categories, function(value, key) {
          categories.push({
            categoryId: value.category_id,
            categoryName: value.category_name,
          });
        });
      }

      res.push({
        name: product._source.name,
        shortDescription: product._source.shortDescription,
        primaryImage: product._source.primary_image,
        reviewRating: product._source.review_rating,
        reviewCount: product._source.review_count,
        storeId: product._source.store_id,
        productId: product._source.product_id,
        mpn: product._source.mpn,
        gtin: product._source.gtin,
        brandName: product._source.brand_name,
        brandId: product._source.brand_id,
        lang: product._source.lang,
        testObject: JSON.stringify(product._source.categories),
        translation: ProductService.translationOutput(
          product._source.translation,
        ),
        allowOutOfStock: product._source.allow_out_of_stock,
        // categories: ProductService.categoriesOutput(
        //   _.toArray(product._source.categories),
        // ),
        categories: categories,
        price: price,
      });
    });
    return res;
  }
  async getProduct(
    data: product.GetProductInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.GetProductResponse> {
    return this.productByStoreModel
      .findOneAsync({
        store_id: data.storeId,
        product_id: timeuuid(data.productId),
      })
      .then(product => {
        if (product === undefined) {
          return {
            exception: {
              message: 'Sorry, product not found',
              statusCode: status.INTERNAL,
            },
          };
        }
        const productData = product.toJSON();
        return {
          product: ProductService.ProductOutput(productData),
          exception: {
            message: 'Success, Item founded',
            statusCode: status.OK,
          },
        };
      });
  }
  static ProductOutput(productData: ProductByStoreEntity): product.Product {
    const price: product.Price[] = [];
    _.each(productData.price, function(value, key) {
      price.push({
        currencyCode: key,
        amount: value,
      });
    });
    const onSale: product.Price[] = [];
    _.each(productData.on_sale_price, function(value, key) {
      onSale.push({
        currencyCode: key,
        amount: value,
      });
    });
    const customFields: Array<product.CustomField> = [];
    _.each(productData.custom_field, function(value, key) {
      customFields.push({
        customFieldId: key,
        required: value,
      });
    });

    const wholeSale: Array<product.WholeSale> = [];
    _.each(productData.whole_sale, function(priceValue, key) {
      const price: Array<product.Price> = [];
      _.each(priceValue, function(pr, key) {
        price.push({
          currencyCode: key,
          amount: pr,
        });
      });
      wholeSale.push({
        quantity: key,
        price: price,
      });
    });

    return {
      productId: productData.product_id,
      name: productData.name[0],
      storeName: productData.store_name[0],
      storeId: productData.store_id,
      shortDescription: productData.short_description[0],
      description: productData.description[0],
      categories: ProductService.categoriesOutput(productData.categories),
      weight: productData.weight,
      quantityDefault: productData.quantity_default,
      quantityMin: productData.quantity_min,
      quantityMax: productData.quantity_max,
      stock: productData.stock,
      stackable: productData.stackable,
      shippable: productData.shippable,
      published: productData.published,
      mpn: productData.mpn,
      gtin: productData.gtin,
      saleCount: productData.sale_count,
      price: price,
      wholeSale: wholeSale,
      onSalePrice: onSale,
      primaryImage: productData.primary_image,
      media: productData.media,
      accessory: productData.accessory,
      brandId: productData.brand_id,
      brandName: productData.brand_name[0],
      customField: customFields,
      reviewCount: productData.review_count,
      reviewRating: productData.review_rating,
      lang: productData.lang,
      variants: ProductService.variantOutput(productData.variants),
      translation: ProductService.translationOutput(productData.translation),
      allowOutOfStock: productData.allow_out_of_stock,
      testObject: JSON.stringify(productData.translation),
    };
  }

  async deleteMedia(
    data: product.DeleteMediaInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.DeleteMediaResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        if (data.media === '') {
          return {
            exception: {
              message: 'please input your media',
              statusCode: status.INTERNAL,
            },
          };
        }
        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            if (data.variantId !== undefined) {
              if (_.has(product.variants, data.variantId)) {
                const variant = _.get(product.variants, data.variantId, {});
                return this.fileService
                  .dispatchUrl(data.media)
                  .then(disRes => {
                    if (disRes.fid === null) {
                      return {
                        exception: {
                          message: 'Sorry, media  not an valid url',
                          statusCode: status.NOT_FOUND,
                        },
                      };
                    }
                    return this.fileService
                      .delete(
                        disRes.volumeId
                          .toString()
                          .concat(',')
                          .concat(disRes.fid.toString()),
                      )
                      .then(res => {
                        if (res === false) {
                          return {
                            exception: {
                              message: 'Sorry, could not delete media variant ',
                              statusCode: status.INTERNAL,
                            },
                          };
                        } else {
                          _.pull(
                            variant.media,
                            disRes.volumeId
                              .toString()
                              .concat(',')
                              .concat(disRes.fid.toString()),
                          );
                          _.assign(variant, { media: variant.media });
                          _.set(product.variants, data.variantId, variant);
                          return product
                            .saveAsync()
                            .then(val => {
                              return {
                                exception: {
                                  message: 'Success, media deleted',
                                  statusCode: status.OK,
                                },
                              };
                            })
                            .catch(err => {
                              return {
                                exception: {
                                  message: 'Sorry, in update media , there is an Error! : '.concat(
                                    _.toString(err),
                                  ),
                                  statusCode: status.INTERNAL,
                                },
                              };
                            });
                        }
                      })
                      .catch(err => {
                        return {
                          exception: {
                            message: 'Sorry in delete varaint media , there is an Error! :'.concat(
                              err,
                            ),
                            statusCode: status.INTERNAL,
                          },
                        };
                      });
                  })
                  .catch(err => {
                    return {
                      exception: {
                        message: 'in dispatch media url, there is an Error! : '.concat(
                          err,
                        ),
                        statusCode: status.INTERNAL,
                      },
                    };
                  });
              } else {
                return {
                  exception: {
                    message: 'There is not the variant',
                    statusCode: status.NOT_FOUND,
                  },
                };
              }
            }

            return this.fileService.dispatchUrl(data.media).then(disRes => {
              if (disRes.fid === null) {
                return {
                  exception: {
                    message: 'Sorry, product media not valid url',
                    statusCode: status.NOT_FOUND,
                  },
                };
              }
              return this.fileService
                .delete(
                  disRes.volumeId
                    .toString()
                    .concat(',')
                    .concat(disRes.fid.toString()),
                )
                .then(delRes => {
                  if (delRes === false) {
                    return {
                      exception: {
                        message: 'Sorry, could not delete product media',
                        statusCode: status.INTERNAL,
                      },
                    };
                  }
                  _.pull(
                    product.media,
                    disRes.volumeId
                      .toString()
                      .concat(',')
                      .concat(disRes.fid.toString()),
                  );

                  return product
                    .saveAsync()
                    .then(val => {
                      return {
                        exception: {
                          message: 'Success,media deleted ',
                          statusCode: status.OK,
                        },
                      };
                    })
                    .catch(err => {
                      return {
                        exception: {
                          message: 'Sorry, in saved media product , Error! : '.concat(
                            err,
                          ),
                          statusCode: status.INTERNAL,
                        },
                      };
                    });
                });
            });
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error! : '.concat(_.toString(err)),
            statusCode: status.INTERNAL,
          },
        };
      });
  }

  async addMedia(
    data: any,
    metadata: Metadata,
    user: any,
    paramsUpload: Array<string> = [],
  ): Promise<product.AddMediaResponse> {
    console.log(data);
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        if (data.media.data === '') {
          return {
            exception: {
              message: 'please input your media',
              statusCode: status.INTERNAL,
            },
          };
        }
        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            if (data.variantId !== undefined) {
              if (_.has(product.variants, data.variantId)) {
                const variant = _.get(product.variants, data.variantId, {});

                return this.fileService
                  .writeFile(data.media.data, '', '', paramsUpload)
                  .then(fid => {
                    if (fid === null) {
                      return {
                        exception: {
                          message: 'Sorry, server could not upload file',
                          statusCode: status.INTERNAL,
                        },
                      };
                    }
                    if (variant.media === null) {
                      variant.media = [];
                    }
                    variant.media.push(fid);

                    _.assign(variant, { media: variant.media });
                    _.set(product.variants, data.variantId, variant);

                    return product
                      .saveAsync()
                      .then(res => {
                        return {
                          exception: {
                            message: 'Success, Variant media  added',
                            statusCode: status.OK,
                          },
                        };
                      })
                      .catch(err => {
                        return {
                          exception: {
                            message: 'Sorry, upload media variant. there is an Error! : '.concat(
                              _.toString(err),
                            ),
                            statusCode: status.INTERNAL,
                          },
                        };
                      });
                  });
              } else {
                return {
                  exception: {
                    message: 'There is not the variant',
                    statusCode: status.NOT_FOUND,
                  },
                };
              }
            }

            return this.fileService
              .writeFile(data.media.data, '', '', paramsUpload)
              .then(fid => {
                if (fid === null) {
                  return {
                    exception: {
                      message: 'Sorry, file system could not update image',
                      statusCode: status.INTERNAL,
                    },
                  };
                }
                if (product.media === null) {
                  product.media = [];
                }
                product.media.push(fid);
                return product
                  .saveAsync()
                  .then(val => {
                    return {
                      exception: {
                        message: 'Success, media updated',
                        statusCode: status.OK,
                      },
                    };
                  })
                  .catch(err => {
                    return {
                      exception: {
                        message: 'Sorry, there is some Error! : '.concat(
                          _.toString(err),
                        ),
                        statusCode: status.INTERNAL,
                      },
                    };
                  });
              });
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error! : '.concat(_.toString(err)),
            statusCode: status.INTERNAL,
          },
        };
      });
  }

  async deletePrimaryImage(
    data: product.DeletePrimaryImageInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.DeletePrimaryImageResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }

        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            if (data.variantId !== undefined) {
              if (_.has(product.variants, data.variantId)) {
                const variant = _.get(product.variants, data.variantId, {});
                if (variant.primary_image !== null) {
                  return this.fileService
                    .delete(variant.primary_image)
                    .then(val => {
                      if (val === true) {
                        variant.primary_image = null;
                        _.assign(variant, { primary_image: null });
                        _.set(product.variants, data.variantId, variant);
                        return product
                          .saveAsync()
                          .then(res => {
                            return {
                              exception: {
                                message: 'success, item updated',
                                statusCode: status.OK,
                              },
                            };
                          })
                          .catch(err => {
                            return {
                              exception: {
                                message: 'Sorry, there is an Error! : '.concat(
                                  _.toString(err),
                                ),
                                statusCode: status.INTERNAL,
                              },
                            };
                          });
                      }
                    })
                    .catch(err => {
                      return {
                        exception: {
                          message: 'there is an Error! : '.concat(
                            _.toString(err),
                          ),
                          statusCode: status.INTERNAL,
                        },
                      };
                    });
                } else {
                  return {
                    exception: {
                      message: 'there is not image in server',
                      statusCode: status.NOT_FOUND,
                    },
                  };
                }
              } else {
                return {
                  exception: {
                    message: 'Sorry,there is not the variant',
                    statusCode: status.NOT_FOUND,
                  },
                };
              }
            }
            return this.fileService.delete(product.primary_image).then(res => {
              if (res === false) {
                return {
                  exception: {
                    message: 'Sorry, file system could not delete image',
                    statusCode: status.INTERNAL,
                  },
                };
              }
              product.primary_image = null;
              return product
                .saveAsync()
                .then(val => {
                  return {
                    exception: {
                      message: 'Success, item updated',
                      statusCode: status.OK,
                    },
                  };
                })
                .catch(err => {
                  return {
                    exception: {
                      message: 'Sorry, there is some Error! : '.concat(
                        _.toString(err),
                      ),
                      statusCode: status.INTERNAL,
                    },
                  };
                });
            });
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error! : '.concat(_.toString(err)),
            statusCode: status.INTERNAL,
          },
        };
      });
  }
  async changePrimaryImage(
    data: product.ChangePrimaryImageInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.ChangePrimaryImageResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        if (data.primaryImage.data === '') {
          return {
            exception: {
              message: 'please input your avatar',
              statusCode: status.INTERNAL,
            },
          };
        }
        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            if (data.variantId !== undefined) {
              if (_.has(product.variants, data.variantId)) {
                const variant = _.get(product.variants, data.variantId, {});
                return this.fileService
                  .writeFile(data.primaryImage.data, '', variant.primary_image)
                  .then(fid => {
                    if (fid === null) {
                      return {
                        exception: {
                          message: 'Sorry, server could not upload file',
                          statusCode: status.INTERNAL,
                        },
                      };
                    }
                    _.assign(variant, { primary_image: fid });
                    _.set(product.variants, data.variantId, variant);
                    return product
                      .saveAsync()
                      .then(res => {
                        return {
                          exception: {
                            message: 'Success, Variant image updated',
                            statusCode: status.OK,
                          },
                        };
                      })
                      .catch(err => {
                        return {
                          exception: {
                            message: 'Sorry, upload image variant. there is an Error! : '.concat(
                              _.toString(err),
                            ),
                            statusCode: status.INTERNAL,
                          },
                        };
                      });
                  });
              } else {
                return {
                  exception: {
                    message: 'There is not the variant',
                    statusCode: status.NOT_FOUND,
                  },
                };
              }
            }
            return this.fileService
              .writeFile(data.primaryImage.data, '', product.primary_image)
              .then(fid => {
                if (fid === null) {
                  return {
                    exception: {
                      message: 'Sorry, file system could not update image',
                      statusCode: status.INTERNAL,
                    },
                  };
                }
                product.primary_image = fid;
                return product
                  .saveAsync()
                  .then(val => {
                    return {
                      exception: {
                        message: 'Success, media updated',
                        statusCode: status.OK,
                      },
                    };
                  })
                  .catch(err => {
                    return {
                      exception: {
                        message: 'Sorry, there is some Error! : '.concat(
                          _.toString(err),
                        ),
                        statusCode: status.INTERNAL,
                      },
                    };
                  });
              });
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error! : '.concat(_.toString(err)),
            statusCode: status.INTERNAL,
          },
        };
      });
  }
  async deleteVariant(
    data: product.DeleteVariantInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.DeleteVariantResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission).then(
      async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            data.VariantIds.forEach(vid => {
              if (_.has(product.variants, vid)) {
                const variant = _.get(product.variants, vid, {});
                if (variant.primary_image !== null) {
                  this.fileService.delete(variant.primary_image);
                }
                variant.media.forEach(media => {
                  this.fileService.delete(media);
                });
                product.variants = _.omit(product.variants, vid);
              }
            });
            return product
              .saveAsync()
              .then(val => {
                return {
                  exception: {
                    message: 'Success, variants removed',
                    statusCode: status.OK,
                  },
                };
              })
              .catch(err => {
                return {
                  exception: {
                    message: 'Sorry, there is an Error! : '.concat(
                      _.toString(err),
                    ),
                    statusCode: status.INTERNAL,
                  },
                };
              });
          });
      },
    );
  }
  async updateVariant(
    data: product.UpdateVariantInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.UpdateVariantResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission).then(
      async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }

            _.set(
              product.variants,
              data.variantId,
              ProductService.variantInput(data.variant.variant),
            );
            return product
              .saveAsync()
              .then(val => {
                return {
                  exception: {
                    message: 'Success, variant updated',
                    statusCode: status.OK,
                  },
                };
              })
              .catch(err => {
                return {
                  exception: {
                    message: 'Sorry, there is an Error! : '.concat(
                      _.toString(err),
                    ),
                    statusCode: status.INTERNAL,
                  },
                };
              });
          });
      },
    );
  }
  async addVariant(
    data: product.AddVariantInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.AddVariantResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission).then(
      async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }

        return this.productByStoreModel
          .findOneAsync({
            store_id: data.storeId,
            product_id: timeuuid(data.productId),
          })
          .then(product => {
            if (product === undefined) {
              return {
                exception: {
                  message: 'Sorry, product not found',
                  statusCode: status.NOT_FOUND,
                },
              };
            }
            const variantId = uuid();
            product.variants =
              product.variants === null ? {} : product.variants;
            _.set(
              product.variants,
              variantId,
              ProductService.variantInput(data.variant.variant),
            );

            return product
              .saveAsync()
              .then(val => {
                return {
                  exception: {
                    message: 'Success, variant added',
                    statusCode: status.OK,
                  },
                  variantId: variantId,
                };
              })
              .catch(err => {
                return {
                  exception: {
                    message: 'Sorry, there is an Error! : '.concat(
                      _.toString(err),
                    ),
                    statusCode: status.INTERNAL,
                  },
                };
              });
          })
          .catch(err => {
            return {
              exception: {
                message: 'Sorry, there is an Error! : '.concat(err),
                statusCode: status.INTERNAL,
              },
            };
          });
      },
    );
  }
  static variantInput(variant: product.variantEdit) {
    const price = {};
    if (typeof variant.price !== 'undefined') {
      variant.price.forEach(pr => {
        _.set(price, pr.currencyCode, pr.amount);
      });
    }

    const onSale = {};
    if (typeof variant.onSalePrice !== 'undefined') {
      variant.onSalePrice.forEach(pr => {
        _.set(onSale, pr.currencyCode, pr.amount);
      });
    }

    const properties = {};
    if (typeof variant.properties !== 'undefined') {
      variant.properties.forEach(pro => {
        _.set(properties, pro.propertyId, {
          property_name: pro.propertyName,
          option_name: pro.optionName,
          option_value: pro.optionValue,
        });
      });
    }

    const wholeSale: Array<any> = [];
    if (typeof variant.wholeSale !== 'undefined') {
      variant.wholeSale.forEach(whole => {
        const price: Array<any> = [];
        whole.price.forEach(pr => {
          _.set(price, pr.currencyCode, pr.amount);
        });
        _.set(wholeSale, whole.quantity, price);
      });
    }

    const custom_field = {};
    if (typeof variant.customField !== 'undefined') {
      variant.customField.forEach(cf => {
        _.set(custom_field, cf.customFieldId, cf.required);
      });
    }

    return {
      name: [variant.name],
      stackable: variant.stackable,
      allow_out_of_stock: variant.allowOutOfStock,
      price: price,
      short_description: [variant.shortDescription],
      description: [variant.description],
      gtin: variant.gtin,
      mpn: variant.mpn,
      stock: variant.stock,
      published: variant.published,
      lang: variant.lang,
      whole_sale: wholeSale,
      on_sale_price: onSale,
      custom_field: custom_field,

      translation: ProductService.translationInput(variant.translation),
      properties: properties,
    };
  }
  static variantOutput(variants): Array<product.Variant> {
    const result: Array<product.Variant> = [];
    if (!_.isObject(variants)) {
      return result;
    }
    _.each(variants, function(variant, key) {
      const price: Array<product.Price> = [];
      _.each(variant.price, function(value, key) {
        price.push({
          currencyCode: key,
          amount: value,
        });
      });
      const properties: Array<product.Properties> = [];
      _.each(variant.properties, function(value, key) {
        properties.push({
          propertyName: value.property_name,
          propertyId: key,
          optionName: value.option_name,
          optionValue: value.option_value,
        });
      });

      const onSale: Array<product.Price> = [];
      _.each(variant.on_sale_price, function(value, key) {
        price.push({
          currencyCode: key,
          amount: value,
        });
      });
      const wholeSale: Array<product.WholeSale> = [];
      _.each(variant.whole_sale, function(priceValue, key) {
        const price: Array<product.Price> = [];
        _.each(priceValue, function(pr, key) {
          price.push({
            currencyCode: key,
            amount: pr,
          });
        });
        wholeSale.push({
          quantity: key,
          price: price,
        });
      });
      const customFields: Array<product.CustomField> = [];
      _.each(variant.custom_field, function(value, key) {
        customFields.push({
          customFieldId: key,
          required: value,
        });
      });
      result.push({
        name: variant.name,
        stackable: variant.stackable,
        allowOutOfStock: variant.allow_out_of_stock,
        price: price,
        wholeSale: wholeSale,
        onSalePrice: onSale,
        shortDescription: variant.short_description,
        description: variant.description,
        gtin: variant.gtin,
        mpn: variant.mpn,
        saleCount: variant.sale_count,
        stock: variant.stock,
        primaryImage: variant.primary_image,
        media: variant.media,
        properties: properties,
        lang: variant.lang,
        translation: ProductService.translationOutput(variant.translation),
        variantId: key,
        customField: customFields,
      });
    });
    return result;
  }
  async updateProduct(
    data: product.UpdateProductInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.UpdateProductResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }

        const store = await this.storeClient
          .getStoreById({ storeId: data.storeId })
          .toPromise();
        const wholeSale: Array<any> = [];
        if (typeof data.product.wholeSale !== 'undefined') {
          data.product.wholeSale.forEach(whole => {
            const price: Array<any> = [];
            whole.price.forEach(pr => {
              _.set(price, pr.currencyCode, pr.amount.toString());
            });
            _.set(wholeSale, whole.quantity, price);
          });
        }

        const onSale = {};
        if (typeof data.product.onSalePrice !== 'undefined') {
          data.product.onSalePrice.forEach(pr => {
            _.set(onSale, pr.currencyCode, pr.amount.toString());
          });
        }

        const price = {};
        if (typeof data.product.price !== 'undefined') {
          data.product.price.forEach(pr => {
            _.set(price, pr.currencyCode, pr.amount.toString());
          });
        }

        const custom_field = {};
        if (typeof data.product.customField !== 'undefined') {
          data.product.customField.forEach(cf => {
            _.set(custom_field, cf.customFieldId, cf.required);
          });
        }

        return this.productByStoreModel
          .updateAsync(
            { store_id: data.storeId, product_id: timeuuid(data.productId) },
            {
              name: [data.product.name],

              store_name: [store.store.name],
              short_description: [data.product.shortDescription],
              description: [data.product.description],
              weight: data.product.weight,
              quantity_default: data.product.quantityDefault,
              quantity_max: data.product.quantityDefault,
              quantity_min: data.product.quantityMin,
              stock: data.product.stock,
              stackable: data.product.stackable,
              shippable: data.product.shippable,
              published: data.product.published,
              custom_field: custom_field,
              mpn: data.product.mpn,
              accessory: data.product.accessory,
              gtin: data.product.gtin,
              allow_out_of_stock: data.product.allowOutOfStock,
              brand_id: uuid(data.product.brandId),
              brand_name: [data.product.brandName],

              translation: ProductService.translationInput(
                data.product.translation,
              ),
              whole_sale: wholeSale,
              lang: data.product.lang,
              address: ProductService.addressInput(store.store.address),
              //  location: [{ ...store.store.address.geoPoint }],
              updated_by: timeuuid(user.userId),
              categories: ProductService.categoriesInput(
                data.product.categories,
              ),
              on_sale_price: onSale,
              price: price,
            },
          )
          .then(value => {
            return {
              exception: {
                message: 'Success, Item updated',
                statusCode: status.OK,
              },
            };
          })
          .catch(err => {
            return {
              exception: {
                message: 'Sorry, there is some Error! : '.concat(
                  _.toString(err),
                ),
              },
            };
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error: '.concat(err),
            statusCode: status.INTERNAL,
          },
        };
      });
  }
  async deleteProduct(
    data: product.DeleteProductInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.DeleteProductResponse> {
    const response: product.DeleteProductResponse = {
      exception: {
        message: 'Success, maybe some product could not delete: => ',
        statusCode: status.OK,
      },
    };
    return new Promise((resolve, reject) => {
      data.productIds.forEach(pid => {
        this.productByStoreModel
          .findOneAsync({ store_id: data.storeId, product_id: timeuuid(pid) })
          .then(product => {
            if (product === undefined) {
              return;
            }
            if (product.primary_image !== null) {
              this.fileService.delete(product.primary_image);
            }
            if (product.media !== null) {
              product.media.forEach(pmedia => {
                this.fileService.delete(pmedia);
              });
            }

            if (product.variants === null) product.variants = [];
            _.toArray(product.variants).forEach(variant => {
              if (variant.primary_image !== null) {
                this.fileService.delete(variant.primary_image);
              }
              if (variant.media !== null) {
                variant.media.forEach(vm => {
                  this.fileService.delete(vm);
                });
              }
            });
            return product
              .deleteAsync()

              .catch(err => {
                response.exception.message.concat(product.product_id);

                resolve(response);
              });
          });
      });
      return resolve(response);
    });
  }
  async addProduct(
    data: product.AddProductInput,
    metadata: Metadata,
    user: any,
  ): Promise<product.AddProductResponse> {
    return this.checkStaff(data.storeId, user.userId, this.permission)
      .then(async checkStaff => {
        if (checkStaff.statusCode !== status.OK) {
          return {
            exception: {
              message: checkStaff.message,
              statusCode: checkStaff.statusCode,
            },
          };
        }
        const store = await this.storeClient
          .getStoreById({ storeId: data.storeId })
          .toPromise();

        const wholeSale: Array<any> = [];
        if (typeof data.product.wholeSale !== 'undefined') {
          data.product.wholeSale.forEach(whole => {
            const price: Array<any> = [];
            whole.price.forEach(pr => {
              _.set(price, pr.currencyCode, pr.amount);
            });
            _.set(wholeSale, whole.quantity, price);
          });
        }

        const onSale = {};
        if (typeof data.product.onSalePrice !== 'undefined') {
          data.product.onSalePrice.forEach(pr => {
            _.set(onSale, pr.currencyCode, pr.amount.toString());
          });
        }

        const price = {};
        if (typeof data.product.price !== 'undefined') {
          data.product.price.forEach(pr => {
            _.set(price, pr.currencyCode, pr.amount.toString());
          });
        }

        const custom_field = {};

        if (typeof data.product.customField !== 'undefined') {
          data.product.customField.forEach(cf => {
            _.set(custom_field, cf.customFieldId, cf.required);
          });
        }
        const productId = timeuuid();
        return new this.productByStoreModel({
          name: [data.product.name],
          store_id: data.product.storeId,
          product_id: productId,
          store_name: [store.store.name],
          short_description: [data.product.shortDescription],
          description: [data.product.description],
          weight: data.product.weight,
          quantity_default: data.product.quantityDefault,
          quantity_max: data.product.quantityMax,
          quantity_min: data.product.quantityMin,
          stock: data.product.stock,
          stackable: data.product.stackable,
          shippable: data.product.shippable,
          published: data.product.published,
          mpn: data.product.mpn,
          gtin: data.product.gtin,
          accessory: data.product.accessory,
          allow_out_of_stock: data.product.allowOutOfStock,
          brand_id: uuid(data.product.brandId),
          brand_name: [data.product.brandName],
          translation: ProductService.translationInput(
            data.product.translation,
          ),
          lang: data.product.lang,
          custom_field: custom_field,
          // address: ProductService.addressInput(store.store.address),
          location: [{ ...store.store.address.geoPoint }],
          categories: ProductService.categoriesInput(data.product.categories),
          created_by: timeuuid(user.userId),
          whole_sale: wholeSale,
          on_sale_price: onSale,
          price: price,
        })
          .saveAsync()
          .then(res => {
            return {
              exception: {
                message: 'Success, product add',
                statusCode: status.OK,
              },
              productId: productId,
            };
          })
          .catch(err => {
            return {
              exception: {
                message: 'Sorry, there is some Error in Saving! : '.concat(err),
              },
            };
          });

        // return new Promise((resolve, reject) => {
        //   data.products.forEach(product => {
        //     let wholeSale: Array<any> = [];
        //     if (typeof product.wholeSale !== 'undefined') {
        //       product.wholeSale.forEach(whole => {
        //         let price: Array<any> = [];
        //         whole.price.forEach(pr => {
        //           _.set(price, pr.currencyCode, pr.amount);
        //         });
        //         _.set(wholeSale, whole.quantity, price);
        //       });
        //     }

        //     let onSale = {};
        //     if (typeof product.onSalePrice !== 'undefined') {
        //       product.onSalePrice.forEach(pr => {
        //         _.set(onSale, pr.currencyCode, pr.amount.toString());
        //       });
        //     }

        //     let price = {};
        //     if (typeof product.price !== 'undefined') {
        //       product.price.forEach(pr => {
        //         _.set(price, pr.currencyCode, pr.amount.toString());
        //       });
        //     }

        //     let custom_field = {};

        //     if (typeof product.customField !== 'undefined') {
        //       product.customField.forEach(cf => {
        //         _.set(custom_field, cf.customFieldId, cf.required);
        //       });
        //     }
        //     new this.productByStoreModel({
        //       name: [product.name],
        //       store_id: product.storeId,
        //       product_id: timeuuid(),
        //       store_name: [store.store.name],
        //       short_description: [product.shortDescription],
        //       description: [product.description],
        //       weight: product.weight,
        //       quantity_default: product.quantityDefault,
        //       quantity_max: product.quantityDefault,
        //       quantity_min: product.quantityMin,
        //       stock: product.stock,
        //       stackable: product.stackable,
        //       shippable: product.shippable,
        //       published: product.published,
        //       mpn: product.mpn,
        //       gtin: product.gtin,
        //       accessory: product.accessory,
        //       allow_out_of_stock: product.allowOutOfStock,
        //       brand_id: uuid(product.brandId),
        //       brand_name: [product.brandName],
        //       translation: ProductService.translationInput(product.translation),
        //       lang: product.lang,
        //       custom_field: custom_field,
        //       address: ProductService.addressInput(store.store.address),
        //       location: [{ ...store.store.address.geoPoint }],
        //       categories: ProductService.categoriesInput(product.categories),
        //       created_by: timeuuid(user.userId),
        //       whole_sale: wholeSale,
        //       on_sale_price: onSale,
        //       price: price,
        //     })
        //       .saveAsync()
        //       .catch(err => {
        //         console.log(err);
        //         response.exception.message.concat(product.name);
        //         resolve(response);
        //       });
        //     resolve(response);
        //   });
        // });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is some Error: '.concat(err),
            statusCode: status.INTERNAL,
          },
        };
      });
  }

  static translationOutput(translations) {
    if (!_.isObject(translations)) {
      return null;
    }
    const result = [];
    _.each(translations.fields, function(value, key) {
      const fieldVal = [];
      _.each(value, function(subval, subkey) {
        fieldVal.push({ name: subkey, value: subval });
      });

      result.push({ name: key, value: fieldVal });
    });
    return result;
  }
  static translationInput(translation) {
    const translateField = {};
    if (typeof translation !== 'undefined' && translation !== null) {
      translation.forEach(trans => {
        const value = {};
        if (typeof trans.value !== 'undefined') {
          trans.value.forEach(val => {
            value[val.name] = val.value;
          });
        }

        translateField[trans.name] = value;
      });
    }

    return {
      fields: translateField,
    };
  }
  static addressInput(address: store.Address): unknown {
    return {};
    if (address !== null) {
      return {
        country_name: address.countryName,
        country_id: address.countryId,
        province_id: address.provinceId,
        province_name: address.provinceName,
        city_id: address.cityId,
        city_name: address.cityName,
        location: { ...address.geoPoint },
        address: address.address,
        zip_code: address.zipCode,
      };
    }
    return {};
  }
  static addressOutput(address: any): product.Address {
    return {
      countryName: address.country_name,
      countryId: address.country_id,
      provinceId: address.province_id,
      provinceName: address.province_name,
      cityId: address.city_id,
      cityName: address.city_name,
      geoPoint: { ...address.location },
      address: address.address,
      zipCode: address.zip_code,
    };
  }
  static categoriesInput(categories: Array<product.Category>): any {
    const categoriesInput: Array<any> = [];
    if (categories !== undefined) {
      categories.forEach(cate => {
        categoriesInput.push({
          category_id: cate.categoryId,
          category_name: cate.categoryName,
          parent_id: cate.parentId,
          primary: cate.primary,
        });
      });
    }

    return categoriesInput;
  }
  static categoriesOutput(categories: any): Array<product.Category> {
    if (categories === null) categories = [];
    const categoriesOutput: Array<product.Category> = [];
    categories.forEach(cate => {
      categoriesOutput.push({
        categoryId: cate.category_id,
        categoryName: cate.category_name,
        parentId: cate.parent_id,
        primary: cate.primary,
      });
    });
    return categoriesOutput;
  }
}
