import { Controller, UseGuards } from '@nestjs/common';
import { ProductService } from './product.service';
import { product } from './proto/product';

import * as grpc from 'grpc';
import {
  GrpcMethod,
  GrpcStreamMethod,
  Payload,
  createGrpcMethodMetadata,
} from '@nestjs/microservices';
import { GRPCUser } from 'src/shared/user.grpc.decorator';
import { GrpcAuthtGuard } from 'src/shared/grpc-auth.guard';
import { Observable } from 'rxjs';

@Controller()
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @GrpcMethod('ProductClient', 'AddProduct')
  @UseGuards(GrpcAuthtGuard)
  async addProduct(
    @Payload() data: product.AddProductInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.AddProductResponse> {
    return this.productService.addProduct(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'UpdateProduct')
  @UseGuards(GrpcAuthtGuard)
  async updateProduct(
    @Payload() data: product.UpdateProductInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.UpdateProductResponse> {
    return this.productService.updateProduct(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'DeleteProduct')
  @UseGuards(GrpcAuthtGuard)
  async deleteProduct(
    @Payload() data: product.DeleteProductInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.DeleteProductResponse> {
    return this.productService.deleteProduct(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'AddVariant')
  @UseGuards(GrpcAuthtGuard)
  async addVariant(
    @Payload() data: product.AddVariantInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.AddVariantResponse> {
    return this.productService.addVariant(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'UpdateVariant')
  @UseGuards(GrpcAuthtGuard)
  async updateVariant(
    @Payload() data: product.UpdateVariantInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.UpdateVariantResponse> {
    return this.productService.updateVariant(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'DeleteVariant')
  @UseGuards(GrpcAuthtGuard)
  async deleteVariant(
    @Payload() data: product.DeleteVariantInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.DeleteVariantResponse> {
    return this.productService.deleteVariant(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'ChangePrimaryImage')
  @UseGuards(GrpcAuthtGuard)
  async changePrimaryImage(
    @Payload() data: product.ChangePrimaryImageInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.ChangePrimaryImageResponse> {
    return this.productService.changePrimaryImage(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'DeletePrimaryImage')
  @UseGuards(GrpcAuthtGuard)
  async DeletePrimaryImage(
    @Payload() data: product.DeletePrimaryImageInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.DeletePrimaryImageResponse> {
    return this.productService.deletePrimaryImage(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'AddMedia')
  @UseGuards(GrpcAuthtGuard)
  async addMedia(
    @Payload() data: product.AddMediaInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.AddMediaResponse> {
    return this.productService.addMedia(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'DeleteMedia')
  @UseGuards(GrpcAuthtGuard)
  async deleteMedia(
    @Payload() data: product.DeleteMediaInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.DeleteMediaResponse> {
    return this.productService.deleteMedia(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'GetProduct')
  @UseGuards(GrpcAuthtGuard)
  async getProduct(
    @Payload() data: product.GetProductInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.GetProductResponse> {
    return this.productService.getProduct(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'RetriveProducts')
  @UseGuards(GrpcAuthtGuard)
  async retriveProducts(
    @Payload() data: product.RetriveProductsInput,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.RetriveProductsResponse> {
    return this.productService.retriveProducts(data, metadata, user);
  }

  @GrpcMethod('ProductClient', 'FindProducts')
  async findProducts(
    @Payload() data: any,
    metadata: grpc.Metadata,
    @GRPCUser() user: any,
  ): Promise<product.FindProductsResponse> {
    return this.productService.findProducts(data, null);
  }
  @GrpcStreamMethod('ProductClient', 'AddMediaStream')
  @UseGuards(GrpcAuthtGuard)
  addMediaStream(data: Observable<any>, metadata: any): Observable<any> {
    console.log(metadata);
    return this.productService.addMediaStream(data, metadata);
  }
}
