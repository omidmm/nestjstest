
/*
 * ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export class FileInput {
    data?: string;
}

export class CreateBrandInput {
    name: string;
    image?: FileInput;
    alias?: string;
    description?: string;
}

export class Pagination {
    currentPage?: number;
    perPage?: number;
    pageState?: string;
}

export class TranslationInput {
    name: string;
    value?: TranslationFieldsInput[];
}

export class TranslationFieldsInput {
    name: string;
    value?: string;
}

export class CreateCategoryInput {
    name: string;
    code?: string;
    description?: string;
    translation?: TranslationInput[];
    sort_order?: number;
    parent_id?: string;
    url?: string;
}

export class CreateCountryInput {
    name: string;
    nameLocale?: string;
    continent?: string;
    lang?: string;
    currency?: string;
    description?: string;
}

export class CreateProvinceInput {
    name: string;
    nameLocale?: string;
    countryName?: string;
    countryId?: string;
}

export class CreateCityInput {
    name: string;
    nameLocale?: string;
    provinceId: string;
    provinceName?: string;
    countryId?: string;
    countryName?: string;
}

export class PropertyOptionInput {
    name: string;
    value?: string;
    index?: string;
    image?: string;
    translation?: TranslationInput[];
}

export class CreatePropertyInput {
    propertyName: string;
    categoryName: string;
    categoryId: string;
    fieldType: string;
    unit?: string;
    options?: PropertyOptionInput[];
    translation?: TranslationInput[];
}

export class DeletePropertyInput {
    categoryId: string;
    propertyId: string;
}

export class AddOptionPropertyInput {
    name: string;
    value?: string;
    index?: string;
    image?: string;
    translation?: TranslationInput[];
}

export class DeleteOptionPropertyInput {
    name: string;
    value?: string;
    index?: string;
    image?: string;
    translation?: TranslationInput[];
}

export class Exception {
    message?: string;
    statusCode?: number;
}

export class Brand {
    brandId?: string;
    name?: string;
    image?: string;
    description?: string;
}

export class BrandCategory {
    categoryId: string;
    brandId: string;
    image?: string;
    name: string;
}

export class GetBrandsResponse {
    brands?: Brand[];
    pageState?: string;
    exception?: Exception;
}

export class GetBrandsByCategoryResponse {
    exception?: Exception;
    pageState?: string;
    brands?: BrandCategory[];
}

export class SearchBrandsResponse {
    exception?: Exception;
    brands?: Brand[];
}

export abstract class IMutation {
    abstract createBrand(brands?: CreateBrandInput[]): Exception | Promise<Exception>;

    abstract deleteBrand(brands?: string[]): Exception | Promise<Exception>;

    abstract removeBrandImage(brandId: string): Exception | Promise<Exception>;

    abstract updateBrand(brandId: string, brand?: CreateBrandInput): Exception | Promise<Exception>;

    abstract addBrandToCategory(categoryId: string, brands?: string[]): Exception | Promise<Exception>;

    abstract removeBrandfromCategory(categoryId: string, brands?: string[]): Exception | Promise<Exception>;

    abstract createCategory(createCategoryInput?: CreateCategoryInput[]): Category | Promise<Category>;

    abstract updateCategory(category_id: string, createCategoryInput?: CreateCategoryInput): Category | Promise<Category>;

    abstract uploadCategoryImage(image: string, category_id: string): Exception | Promise<Exception>;

    abstract deleteCategory(category_id?: string): Exception | Promise<Exception>;

    abstract addCountry(createCountryInput?: CreateCountryInput[]): Exception | Promise<Exception>;

    abstract updateCountry(countryId: string, createCountryInput?: CreateCountryInput): Exception | Promise<Exception>;

    abstract deleteCountry(countryId?: string): Exception | Promise<Exception>;

    abstract addProvince(createProvinceInput?: CreateProvinceInput[]): Exception | Promise<Exception>;

    abstract updateProvince(provinceId: string, createProvinceInput?: CreateProvinceInput): Exception | Promise<Exception>;

    abstract deleteProvince(provinceId: string, countryId: string): Exception | Promise<Exception>;

    abstract addCity(createCityInput?: CreateCityInput[]): Exception | Promise<Exception>;

    abstract updateCity(cityId: string, createCityInput?: CreateCityInput): Exception | Promise<Exception>;

    abstract deleteCity(cityId: string, provinceId: string): Exception | Promise<Exception>;

    abstract addProperty(addPropertyInput?: CreatePropertyInput[]): Exception | Promise<Exception>;

    abstract updateProperty(propertyId: string, categoryId: string, updatePropertyInput?: CreatePropertyInput): Exception | Promise<Exception>;

    abstract deleteProperty(deletePropertyInput?: DeletePropertyInput[]): Exception | Promise<Exception>;

    abstract addOptionProperty(categoryId: string, propertyId: string, addOptionPropertyInput?: AddOptionPropertyInput[]): Exception | Promise<Exception>;

    abstract updateOptionProperty(categoryId: string, propertyId: string, updateOptionPropertyInput?: AddOptionPropertyInput): Exception | Promise<Exception>;

    abstract deleteOptionProperty(categoryId: string, propertyId: string, deleteOptionPropertyInput?: DeleteOptionPropertyInput[]): Exception | Promise<Exception>;
}

export abstract class IQuery {
    abstract getBrands(pagination?: Pagination): GetBrandsResponse | Promise<GetBrandsResponse>;

    abstract getBrandsByCategory(categoryId: string, pagination?: Pagination): GetBrandsByCategoryResponse | Promise<GetBrandsByCategoryResponse>;

    abstract searchBrands(name: string, pagination?: Pagination): SearchBrandsResponse | Promise<SearchBrandsResponse>;

    abstract findCategory(category_id?: string): Category | Promise<Category>;

    abstract findAllCategories(): Category[] | Promise<Category[]>;

    abstract findCountry(countryId: string): FindCountryResponse | Promise<FindCountryResponse>;

    abstract findAllCountries(): FindAllCountriesResponse | Promise<FindAllCountriesResponse>;

    abstract findAllProvince(countryId: string): FindAllProvinceResponse | Promise<FindAllProvinceResponse>;

    abstract findAllCity(provinceId: string): FindAllCityResponse | Promise<FindAllCityResponse>;

    abstract findProperty(propertyId: string, categoryId: string): FindPropertyResponse | Promise<FindPropertyResponse>;

    abstract findPropertiesByCategory(categoryId: string): FindPropertiesByCategoryResponse | Promise<FindPropertiesByCategoryResponse>;
}

export class Category {
    category_id?: string;
    name: string;
    code?: string;
    description?: string;
    sort_order?: number;
    url?: string;
    image?: string;
    translation?: Translation[];
    parent_id?: string;
    children?: Category[];
    exception?: Exception;
}

export class Translation {
    name: string;
    value?: TranslationFields[];
}

export class TranslationFields {
    name: string;
    value?: string;
}

export class Country {
    name: string;
    nameLocale?: string;
    countryId?: string;
    continent?: string;
    lang?: string;
    currency?: string;
    description?: string;
}

export class Province {
    provinceId?: string;
    name?: string;
    nameLocale?: string;
    countryName?: string;
    countryId?: string;
}

export class City {
    cityId?: string;
    name?: string;
    nameLocale?: string;
    provinceId?: string;
    provinceName?: string;
    countryId?: string;
    countryName?: string;
}

export class FindCountryResponse {
    country?: Country;
    exception?: Exception;
}

export class FindAllCountriesResponse {
    countries?: Country[];
    exception?: Exception;
}

export class FindAllProvinceResponse {
    provinces?: Province[];
    exception?: Exception;
}

export class FindAllCityResponse {
    cities?: City[];
    exception?: Exception;
}

export class PropertyOptions {
    name?: string;
    value?: string;
    index?: string;
    image?: string;
    translation?: Translation[];
}

export class Property {
    categoryId?: string;
    propertyId?: string;
    categoryName?: string;
    propertyName?: string;
    fieldType?: string;
    unit?: string;
    fav?: number;
    options?: PropertyOptions[];
    translation?: Translation[];
}

export class FindPropertyResponse {
    property?: Property;
    exception?: Exception;
}

export class FindPropertiesByCategoryResponse {
    properties?: Property[];
    exception?: Exception;
}

export type Upload = any;
