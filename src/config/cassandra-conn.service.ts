import { Injectable } from '@nestjs/common';
import {
  ExpressCassandraOptionsFactory,
  ExpressCassandraModuleOptions,
  auth,
} from '@iaminfinity/express-cassandra';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class CassandraConnService {
  // constructor(private readonly configService: ConfigService) {}
  // createExpressCassandraOptions():
  //   | ExpressCassandraModuleOptions
  //   | Promise<ExpressCassandraModuleOptions> {
  //   return this.getDbConfig1();

  // }
  static getDbConfig(configService: ConfigService): any {
    //  ExpressCassandraModuleOptions
    return {
      clientOptions: {
        elasticsearch: {
          //   host: 'http://localhost:9200',
          // apiVersion: '5.5',
          sniffOnStart: false,
        },

        contactPoints: configService
          .get<string>('CASSANDRA_HOST_POINTS')
          .split(','),
        keyspace: configService.get<string>('CASSANDRA_KEYSPACE'),
        authProvider: new auth.PlainTextAuthProvider(
          configService.get<string>('CASSANDRA_USERNAME'),
          configService.get<string>('CASSANDRA_PASSWORD'),
        ),
        protocolOptions: {
          port: configService.get<number>('CASSANDRA_PORT'),
        },
        queryOptions: {
          consistency: 1,
        },
      },
      ormOptions: {
        createKeyspace: true,

        defaultReplicationStrategy: {
          class: 'NetworkTopologyStrategy',
          DC1: 1,
        },
        udts: {
          geo_point: {
            lat: 'double',
            lon: 'double',
          },
          category: {
            category_id: 'varchar',
            category_name: 'varchar',
            parent_id: 'varchar',
            primary: 'boolean',
          },
          address: {
            country_name: 'varchar',
            country_id: 'varchar',
            province_name: 'varchar',
            province_id: 'varchar',
            city_name: 'varchar',
            city_id: 'varchar',
            address: 'text',
            zip_code: 'varchar',
            location: 'frozen<geo_point>',
          },

          translation: {
            fields: 'map<varchar, frozen<map<varchar,text >> >',
          },
          property_options: {
            name: 'varchar',
            value: 'varchar',
            index: 'varchar',
            image: 'text',
            translation: 'frozen<translation>',
          },
          custom_field_options: {
            name: 'varchar',
            value: 'varchar',
            index: 'varchar',
            default: 'boolean',
            image: 'text',
            description: 'text',
            price: 'map<varchar,varchar>',
            translation: 'frozen<translation>',
          },
          variant_property: {
            property_name: 'varchar',
            option_name: 'varchar',
            option_value: 'varchar',
          },
          variant: {
            name: 'list<text>',
            stackable: 'boolean',
            allow_out_of_stock: 'boolean',
            price: 'map<varchar,decimal>',
            on_sale_price: 'map<varchar,decimal>',
            media: 'set<text>',
            primary_image: 'varchar',
            published: 'boolean',
            short_description: 'list<text>',
            description: 'list<text>',
            gtin: 'varchar',
            mpn: 'varchar',
            sale_count: 'int',
            stock: 'int',
            lang: 'varchar',
            custom_field: 'map<varchar,boolean>',
            translation: 'frozen<translation>',
            whole_sale: 'map<varchar,frozen<map<varchar,decimal>>>',
            properties: 'map<varchar,frozen<variant_property>>',
          },
        },
        // omitted other options for clarity
        migration: 'alter',
        disableTTYConfirmation: true,
        manageESIndex: true,
      },
    };
  }
}
