import { Controller, Post, Body } from '@nestjs/common';
import { SmsService } from 'src/shared/sms.service';

@Controller('test')
export class TestController {
  constructor(private smsSerive: SmsService) {}

  @Post('sendSms')
  async sendSmsTest(
    @Body('phone_number') phone_number,
    @Body('message') message,
  ) {
    message = 'این پیام از سرویس پیامکی و پست من است';
    phone_number = '09373930035';
    return this.smsSerive.send(phone_number, message);
  }
}
