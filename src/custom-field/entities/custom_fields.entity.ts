import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'custom_field',
  key: ['store_id', 'cf_id'],
  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
})
export class CustomFieldEntity {
  @Column({
    type: 'varchar',
  })
  store_id: any;

  @Column({ type: 'timeuuid' })
  cf_id: any;

  @Column({
    type: 'varchar',
  })
  name: string;

  @Column({
    type: 'text',
  })
  description: string;

  @Column({
    type: 'boolean',
    default: () => {
      return false;
    },
  })
  required: boolean;

  @Column({
    type: 'map',
    typeDef: '<varchar,varchar>',
  })
  price: object;

  @Column({
    type: 'varchar',
  })
  field_type: string;

  @Column({
    type: 'int',
    default: () => {
      return 1;
    },
  })
  fav: number;

  @Column({
    type: 'map',
    typeDef: '<varchar,frozen<custom_field_options>>',
  })
  options: any;
  @Column({
    type: 'frozen',
    typeDef: '<translation>',
  })
  translation: any;
  @CreateDateColumn()
  created_at: Date;
  @UpdateDateColumn()
  updated_at: Date;
}
