import { Injectable } from '@nestjs/common';
import {
  InjectModel,
  BaseModel,
  timeuuid,
} from '@iaminfinity/express-cassandra';
import { CustomFieldEntity } from './entities/custom_fields.entity';
import { customfield } from './proto/cf';
import * as _ from 'lodash';
import { SeaweedfsService } from 'src/shared/seaweedfs.service';
import * as grpc from 'grpc';

@Injectable()
export class CustomFieldService {
  async removeOptionFromCustomField(
    data: customfield.RemoveOptionFromCustomFieldInput,
  ): Promise<customfield.RemoveOptionFromCustomFieldResponse> {
    if (data.cfId === undefined || data.storeId === undefined) {
      return {
        exception: {
          message: 'Please set custom field Id and store Id',
          statusCode: grpc.status.NOT_FOUND,
        },
      };
    }
    return this.cfModel
      .findOneAsync({ cf_id: timeuuid(data.cfId), store_id: data.storeId })
      .then(cf => {
        if (cf === undefined) {
          return {
            exception: {
              message: 'Sorry, Item not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }
        data.options.forEach(option => {
          if (_.has(cf.options, option.name)) {
            const oldOption = _.get(cf.options, option.name, {});
            if (oldOption.image !== null) {
              this.fileService.delete(oldOption.image);
            }
            cf.options = _.omit(cf.options, option.name);
          }
        });
        return cf.saveAsync().then(() => {
          return {
            exception: {
              message: 'Item updated',
              statusCode: grpc.status.OK,
            },
          };
        });
      });
  }
  async addOptionToCustomField(
    data: customfield.AddOptionToCustomFieldInput,
  ): Promise<customfield.AddCustomToCustomFieldResponse> {
    if (data.cfId === undefined || data.storeId === undefined) {
      return {
        exception: {
          message: 'Please set custom field Id and store Id',
          statusCode: grpc.status.NOT_FOUND,
        },
      };
    }
    return this.cfModel
      .findOneAsync({ cf_id: timeuuid(data.cfId), store_id: data.storeId })
      .then(async cf => {
        if (cf === undefined) {
          return {
            exception: {
              message: 'Sorry, Item not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }

        if (_.size(cf.options) === 0) cf.options = {};
        for await (const option of data.options) {
          const oldOption = _.get(cf.options, option.name, {});
          _.set(
            cf.options,
            option.name,
            await this.optionsInput(option, oldOption),
          );
        }
        //   data.options.map(async option => {
        //   const oldOption = _.get(cf.options, option.name, {});
        //   _.set(
        //     cf.options,
        //     option.name,
        //     await this.optionsInput(option, oldOption),
        //   );
        // });

        //  console.log(cf.options);

        return cf
          .saveAsync()
          .then(() => {
            return {
              exception: {
                message: 'Success, Item saved',
                statusCode: grpc.status.OK,
              },
            };
          })
          .catch(err => {
            return {
              exception: {
                message: 'sorry, Item not saved'.concat(_.toString(err)),
                statusCode: grpc.status.OK,
              },
            };
          });

        // return new Promise((resolve, reject) => {
        //   if (_.size(cf.options) === 0) cf.options = {};
        //     data.options.map(async option => {
        //     const oldOption = _.get(cf.options, option.name, {});
        //     _.set(
        //       cf.options,
        //       option.name,
        //       await this.optionsInput(option, oldOption),
        //     );
        //     console.log(cf.options);
        //   });
        //   return resolve(cf);
        // }).then(res => {
        //   console.log(cf.options);
        //   return cf
        //     .saveAsync()
        //     .then(cfsaved => {
        //       return {
        //         exception: {
        //           message: 'Success, Item saved',
        //           statusCode: grpc.status.OK,
        //         },
        //       };
        //     })
        //     .catch(err => {
        //       return {
        //         exception: {
        //           message: 'sorry, Item not saved'.concat(_.toString(err)),
        //           statusCode: grpc.status.OK,
        //         },
        //       };
        //     });

        // });
      });
  }
  /**
   * @name translationOutput
   * @description has created for converting object to Translation Interface
   * @param translations translation objects
   */
  static translationOutput(translations) {
    if (!_.isObject(translations)) {
      return null;
    }
    const result = [];
    _.each(translations.fields, function(value, key) {
      const fieldVal = [];
      _.each(value, function(subval, subkey) {
        fieldVal.push({ name: subkey, value: subval });
      });

      result.push({ name: key, value: fieldVal });
    });
    return result;
  }

  async optionOutput(options): Promise<Array<customfield.Options>> {
    const result: Array<customfield.Options> = [];
    if (!_.isObject(options)) {
      return result;
    }
    _.each(options, async function(option) {
      const price: Array<customfield.Price> = [];
      _.each(option.price, function(value, key) {
        price.push({
          currencyCode: key,
          amount: value,
        });
      });
      result.push({
        name: option.name,
        index: option.index,
        value: option.value,
        description: option.value,
        price: price,
        image: option.image,
        translation: CustomFieldService.translationOutput(option.translation),
      });
    });
    return result;
  }
  /**
   * @name translationInput
   * @description has created for convertion array to object for storing id database
   * @param translations translation Array
   */
  static translationInput(translation: customfield.Translation[]) {
    const translateField = {};

    translation.forEach(trans => {
      const value = {};
      trans.value.forEach(val => {
        value[val.name] = val.value;
      });
      translateField[trans.name] = value;
    });

    return {
      fields: translateField,
    };
  }

  async optionsInput(option: customfield.Options, oldOption) {
    let image = null;
    if (oldOption.image !== undefined) {
      image = await this.fileService.writeFile(
        option.image,
        '',
        oldOption.image,
      );
    } else {
      image = await this.fileService.writeFile(option.image);
    }
    const price = {};
    option.price.forEach(pr => {
      _.set(price, pr.currencyCode, pr.amount.toString());
    });
    return {
      name: option.name,
      value: option.value,
      index: option.index,
      default: option.default,
      description: option.description,
      image: _.toString(image),
      price: price,
      translation: CustomFieldService.translationInput(option.translation),
    };
  }

  async addCustomField(
    data: customfield.AddCustomFieldInput,
  ): Promise<customfield.AddCustomFieldResponse> {
    const price = {};
    data.price.forEach(pr => {
      _.set(price, pr.currencyCode, pr.amount.toString());
    });
    return new this.cfModel({
      name: data.name,
      store_id: data.storeId,
      cf_id: timeuuid(),
      description: data.description,
      field_type: data.fieldType,
      price: price,
      translation: CustomFieldService.translationInput(data.translation),
    })
      .saveAsync()
      .then(value => {
        if (value !== undefined) {
          return {
            exception: {
              message: 'Success, Item saved',
              statusCode: grpc.status.OK,
            },
          };
        }
      })
      .catch(err => {
        return {
          exception: {
            message: _.toString(err),
            statusCode: grpc.status.INTERNAL,
          },
        };
      });
  }
  async deleteCustomField(
    data: customfield.DeleteCustomFieldInput,
  ): Promise<customfield.DeleteCustomFieldResponse> {
    if (data.cfId === undefined || data.storeId === undefined) {
      return {
        exception: {
          message: 'Sorry, please set custom field id and store id',
          statusCode: grpc.status.NOT_FOUND,
        },
      };
    }
    return this.cfModel
      .findOneAsync({ cf_id: timeuuid(data.cfId), store_id: data.storeId })
      .then(cf => {
        if (cf === undefined) {
          return {
            exception: {
              message: 'Sorry, Custom field not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }
        if (cf.options === null) cf.options = [];

        _.toArray(cf.options).forEach(option => {
          if (option.image !== null) {
            this.fileService.delete(option.image);
          }
        });

        return cf.deleteAsync().then(() => {
          return {
            exception: {
              message: 'Success, Item deleted',
              statusCode: grpc.status.OK,
            },
          };
        });
      });
  }
  async retriveCustomFields(
    data: customfield.RetriveCustomFieldsInput,
  ): Promise<customfield.RetriveCustomFieldResponse> {
    return this.cfModel
      .findAsync({ store_id: data.storeId }, { allow_filtering: true })
      .then(items => {
        const resOptions: customfield.CustomField[] = [];
        items.forEach(async item => {
          const price: Array<customfield.Price> = [];
          _.each(item.price, function(value, key) {
            price.push({
              currencyCode: key,
              amount: value,
            });
          });

          resOptions.push({
            name: item.name,
            cfId: item.cf_id,
            storeId: item.store_id,
            description: item.description,
            required: item.required,
            price: price,
            fav: item.fav,
            fieldType: item.field_type,
            translation: CustomFieldService.translationOutput(item.translation),
            options: await this.optionOutput(item.options),
          });
        });

        return {
          customFields: resOptions,
          exception: {
            message: 'Success, items founded',
            statusCode: grpc.status.OK,
          },
        };
      });
  }
  async updateCustomField(
    data: customfield.UpdateCustomFieldInput,
  ): Promise<customfield.UpdateCustomFieldResponse> {
    return this.cfModel
      .findOneAsync({ cf_id: timeuuid(data.cfId), store_id: data.storeId })
      .then(cf => {
        if (cf === undefined) {
          return {
            exception: {
              message: 'Sorry, Item not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }
        const price = cf.price;
        data.price.forEach(pr => {
          _.set(price, pr.currencyCode, pr.amount.toString());
        });
        return this.cfModel
          .updateAsync(
            { store_id: cf.store_id, cf_id: timeuuid(cf.cf_id) },
            {
              name: data.name,
              description: data.description,
              field_type: data.fieldType,
              price: price,
              required: data.required,
              translation: CustomFieldService.translationInput(
                data.translation,
              ),
            },
          )
          .catch(err => {
            return {
              exception: {
                message: _.toString(err),
                statusCode: grpc.status.INTERNAL,
              },
            };
          })
          .then(val => {
            if (val !== undefined) {
              return {
                exception: {
                  message: 'Success, Item updated',
                  statusCode: grpc.status.OK,
                },
              };
            }
          });
      });
  }
  constructor(
    @InjectModel(CustomFieldEntity)
    private readonly cfModel: BaseModel<CustomFieldEntity>,
    private readonly fileService: SeaweedfsService,
  ) {}
}
