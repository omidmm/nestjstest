import { Module } from '@nestjs/common';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { SharedModule } from 'src/shared/shared.module';
import { CustomFieldService } from './custom-field.service';
import { CustomFieldController } from './custom-field.controller';
import { CustomFieldEntity } from './entities/custom_fields.entity';

@Module({
  imports: [
    ExpressCassandraModule.forFeature([CustomFieldEntity]),
    SharedModule,
  ],
  controllers: [CustomFieldController],
  providers: [CustomFieldService],
})
export class CustomFieldModule {}
