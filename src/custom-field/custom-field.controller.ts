import { Controller } from '@nestjs/common';
import { CustomFieldService } from './custom-field.service';
import { GrpcMethod, Payload } from '@nestjs/microservices';
import { customfield } from './proto/cf';
import * as grpc from 'grpc';

@Controller()
export class CustomFieldController {
  constructor(private readonly customFieldService: CustomFieldService) {}

  @GrpcMethod('CustomFieldClient', 'AddCustomField')
  async addCustomField(
    @Payload() data: customfield.AddCustomFieldInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.AddCustomFieldResponse> {
    return this.customFieldService.addCustomField(data);
  }

  @GrpcMethod('CustomFieldClient', 'UpdateCustomField')
  async updateCustomField(
    @Payload() data: customfield.UpdateCustomFieldInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.UpdateCustomFieldResponse> {
    return this.customFieldService.updateCustomField(data);
  }

  @GrpcMethod('CustomFieldClient', 'AddOptionToCustomField')
  async addOptionToCustomField(
    @Payload() data: customfield.AddOptionToCustomFieldInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.AddCustomToCustomFieldResponse> {
    return this.customFieldService.addOptionToCustomField(data);
  }

  @GrpcMethod('CustomFieldClient', 'RemoveOptionFromCustomField')
  async removeOptionFromCustomField(
    @Payload() data: customfield.RemoveOptionFromCustomFieldInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.RemoveOptionFromCustomFieldResponse> {
    return this.customFieldService.removeOptionFromCustomField(data);
  }

  @GrpcMethod('CustomFieldClient', 'RetriveCustomFields')
  async retriveCustomFields(
    @Payload() data: customfield.RetriveCustomFieldsInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.RetriveCustomFieldResponse> {
    return this.customFieldService.retriveCustomFields(data);
  }

  @GrpcMethod('CustomFieldClient', 'DeleteCustomField')
  async deleteCustomField(
    @Payload() data: customfield.DeleteCustomFieldInput,
    metadata: grpc.Metadata,
  ): Promise<customfield.DeleteCustomFieldResponse> {
    return this.customFieldService.deleteCustomField(data);
  }
}
