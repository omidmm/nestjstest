/**
 * This file is auto-generated by nestjs-proto-gen-ts
 */

import { Observable } from 'rxjs';
import { Metadata } from 'grpc';

export namespace customfield {
  export interface Exception {
    message?: string;
    statusCode?: number;
  }
  export interface TranslationFields {
    name?: string;
    value?: string;
  }
  export interface Translation {
    name?: string;
    value?: customfield.TranslationFields[];
  }
  export interface Price {
    currencyCode?: string;
    amount?: string;
  }
  export interface Options {
    name?: string;
    value?: string;
    index?: string;
    image?: string;
    description?: string;
    default?: boolean;
    price?: customfield.Price[];
    translation?: customfield.Translation[];
  }
  export interface CustomField {
    cfId?: string;
    storeId?: string;
    name?: string;
    description?: string;
    required?: boolean;
    price?: customfield.Price[];
    fieldType?: string;
    fav?: number;
    options?: customfield.Options[];
    translation?: customfield.Translation[];
  }
  export interface AddCustomFieldInput {
    storeId?: string;
    name?: string;
    description?: string;
    required?: boolean;
    price?: customfield.Price[];
    fieldType?: string;
    translation?: customfield.Translation[];
  }
  export interface AddCustomFieldResponse {
    exception?: customfield.Exception;
  }
  export interface UpdateCustomFieldInput {
    storeId?: string;
    cfId?: string;
    name?: string;
    description?: string;
    required?: boolean;
    price?: customfield.Price[];
    fieldType?: string;
    translation?: customfield.Translation[];
  }
  export interface UpdateCustomFieldResponse {
    exception?: customfield.Exception;
  }
  export interface AddOptionToCustomFieldInput {
    storeId?: string;
    cfId?: string;
    options?: customfield.Options[];
  }
  export interface AddCustomToCustomFieldResponse {
    exception?: customfield.Exception;
  }
  export interface RemoveOptionFromCustomFieldInput {
    storeId?: string;
    cfId?: string;
    options?: customfield.Options[];
  }
  export interface RemoveOptionFromCustomFieldResponse {
    exception?: customfield.Exception;
  }
  export interface RetriveCustomFieldsInput {
    storeId?: string;
  }
  export interface RetriveCustomFieldResponse {
    customFields?: customfield.CustomField[];
    exception?: customfield.Exception;
  }
  export interface DeleteCustomFieldInput {
    storeId?: string;
    cfId?: string;
  }
  export interface DeleteCustomFieldResponse {
    exception?: customfield.Exception;
  }
  export interface CustomFieldClient {
    addCustomField(
      data: AddCustomFieldInput,
      metadata?: Metadata,
    ): Observable<AddCustomFieldResponse>;
    addOptionToCustomField(
      data: AddOptionToCustomFieldInput,
      metadata?: Metadata,
    ): Observable<AddCustomToCustomFieldResponse>;
    removeOptionFromCustomField(
      data: RemoveOptionFromCustomFieldInput,
      metadata?: Metadata,
    ): Observable<RemoveOptionFromCustomFieldResponse>;
    updateCustomField(
      data: UpdateCustomFieldInput,
      metadata?: Metadata,
    ): Observable<UpdateCustomFieldResponse>;
    retriveCustomFields(
      data: RetriveCustomFieldsInput,
      metadata?: Metadata,
    ): Observable<RetriveCustomFieldResponse>;
    deleteCustomField(
      data: DeleteCustomFieldInput,
      metadata?: Metadata,
    ): Observable<DeleteCustomFieldResponse>;
  }
}
