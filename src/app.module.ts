import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import { GqlConfigService } from './gql-config.service';
import { CategoryModule } from './category/category.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { CassandraConnService } from './config/cassandra-conn.service';
import { LocationModule } from './location/location.module';
import { StoreModule } from './store/store.module';
import { PropertiesModule } from './properties/properties.module';
import { BrandModule } from './brand/brand.module';
import { CustomFieldModule } from './custom-field/custom-field.module';
import { DiscountModule } from './discount/discount.module';
import { ProductModule } from './product/product.module';
import { SharedModule } from './shared/shared.module';
import { RedisModule } from 'nestjs-redis';
import { join } from 'path';
@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ['.env.development', '.env.production'],
    }),
    GraphQLModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        playground: Boolean(configService.get('GRAPHQL_PLAYGROUND')),
        typePaths: ['./**/*.graphql'],
        context: ({ req }) => ({ headers: req.headers }),
        definitions: {
          path: join(process.cwd(), 'src/graphql.ts'),
          outputAs: 'class',
        },
      }),
    }),
    RedisModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        host: configService.get<string>('REDIS_HOST'),
        port: configService.get<number>('REDIS_PORT'),
        password: configService.get<string>('REDIS_PASSWORD'),
        db: configService.get<number>('REDIS_DB'),
        keyPrefix: configService.get<string>('REDIS_PREFIX'),
      }),
    }),
    ExpressCassandraModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return CassandraConnService.getDbConfig(configService);
      },
    }),
    CategoryModule,
    LocationModule,
    StoreModule,
    SharedModule,
    PropertiesModule,
    BrandModule,
    CustomFieldModule,
    DiscountModule,
    ProductModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
