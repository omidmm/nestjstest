import { Observable } from 'rxjs';

export interface WriteTokenRes {
  statusCode: number;
  token: string;
}
export interface GetTokenInput {
  token: string;
}
export interface GetTokenRes {
  statusCode: number;
  message: string;
  userId: string;
  username: string;
}
export interface WriteTokenInput {
  userId: string;
  username: string;
}

export interface AuthClient {
  writeToken(request: WriteTokenInput): Observable<WriteTokenRes>;
  getToken(request: GetTokenInput): Observable<GetTokenRes>;
}
