import { Injectable, Logger } from '@nestjs/common';
import Axios, { AxiosResponse, AxiosRequestConfig } from 'axios';
import * as FormData from 'form-data';
import { ConfigService } from '@nestjs/config';
import { isNull } from 'util';
import { arrayContains, isBase64 } from 'class-validator';

interface urlData {
  volumeId: number;
  fid: string;
}
interface locationType {
  volumeId: number;
  url: string;
  publicUrl: string;
  fid?: string;
  auth?: string;
}
interface NewId {
  count?: number;
  fid: string;
  url: string;
  publicUrl: string;
  auth?: string;
}
interface WriteChunkResponse {
  etag?: string;
  fid?: string;
  fileName?: string;
  fileUrl?: string;
  size?: number;
}
@Injectable()
/**
 *  this package created for upload file by seaweed file system
 */
export class SeaweedfsService {
  private masterUrl: string;

  constructor(private readonly configService: ConfigService) {
    this.masterUrl = 'http://'.concat(
      this.configService.get<string>('SEAWEEDFS_MASTER_POINTS'),
    );
  }
  /**
   * @description this method has created for finding  location of file
   * @param url that has stored in db
   */
  private async findLocation(url: string | number): Promise<locationType> {
    // let volumeId: number;
    // if (typeof url === 'number') {
    //   volumeId = url;
    // } else {
    //   volumeId = (await this.dispatchUrl(url)).volumeId;
    // }
    // if (!isNaN(url)) {
    const location = await Axios.get(
      this.masterUrl.concat('/dir/lookup?fileId=').concat(url.toString()),
    );
    return {
      volumeId: location.data.volumeId,
      publicUrl: 'http://'.concat(location.data.locations[0].publicUrl),
      //  publicUrl: this.configService.get<string>('Seaweedfs_Volume_Url'),

      url: location.data.locations[0].url,
      // url: this.configService.get<string>('Seaweedfs_Volume_Url'),
      auth:
        location.headers['authorization'] !== undefined
          ? location.headers['authorization']
          : null,
    };
    // }
    // return {
    //   volumeId: 0,
    //   publicUrl: null,

    //   url: null,
    // };
  }
  /**
   * @description for dispatch fid , volume id
   * @param url is string format ,that contains volumeid , fid
   */
  async dispatchUrl(url: string): Promise<urlData> {
    const urlRes = url.split(',', 2);
    return {
      volumeId:
        !isNull(parseInt(urlRes[0])) && urlRes[0] !== undefined
          ? parseInt(urlRes[0].slice(-1))
          : 0,
      fid: !isNull(urlRes[1]) && urlRes[1] !== undefined ? urlRes[1] : null,
    };
  }
  /**
   * get new id by master node of seaweed
   */
  private async getNewId(): Promise<NewId> {
    const newId = await Axios.get(this.masterUrl.concat('/dir/assign'));
    return {
      count: newId.data.count !== undefined ? newId.data.count : 0,
      publicUrl: 'http://'.concat(newId.data.publicUrl),
      url: newId.data.url,
      fid: newId.data.fid !== undefined ? newId.data.fid : null,
      auth:
        newId.headers['authorization'] !== undefined
          ? newId.headers['authorization']
          : null,
    };
  }
  /**
   *
   * @param fileData binary or base64 or buffer format
   * @param type format of file data , that we shall create blob file by type
   * @param updateUrl if have updateUrl , we should update file data
   */
  async writeFile(
    fileData,
    type = '',
    updateUrl: string = null,
    params: Array<string> = [],
  ): Promise<string> {
    if (fileData === null) {
      return null;
    }
    const form_data = new FormData();
    let newFid = null;
    let file = null;
    let paramString = '?';
    params.forEach(param => {
      paramString += param.concat('&');
    });

    if (isBase64(fileData)) {
      file = Buffer.from(fileData, 'base64');
    } else if (Buffer.isBuffer(fileData)) {
      file = fileData;
    } else {
      return null;
    }
    form_data.append('file', file);

    const formHeaders = form_data.getHeaders();
    let auth = '';

    let sendUrl: string = null;
    if (!isNull(updateUrl)) {
      const loc = await this.findLocation(updateUrl);
      sendUrl = !isNull(loc.publicUrl) ? `${loc.publicUrl}/${updateUrl}` : null;
      newFid = updateUrl;
      auth = loc.auth;
    }
    if (isNull(sendUrl)) {
      const newId = await this.getNewId();
      sendUrl =
        !isNull(newId.publicUrl) && !isNull(newId.fid)
          ? `${newId.publicUrl}/${newId.fid}`.concat(paramString)
          : null;
      newFid = newId.fid;
      auth = newId.auth;
    }
    if (!isNull(sendUrl)) {
      return Axios.post(sendUrl, form_data, {
        headers: {
          ...formHeaders,
          Authorization: auth,
        },
      })
        .then(uploadFile => {
          return uploadFile.status === 201 || uploadFile.status === 204
            ? newFid
            : null;
        })
        .catch(err => {
          return null;
        });
    }
    return null;
  }
  /**
   * @name readFile
   * @description with this method we can find location of file in file system
   * @param url is string type that has stored in database
   */
  async readFile(url: string): Promise<string> {
    if (url === null || url === undefined || url === '') {
      return null;
    }
    // const dispatchUrl = await this.dispatchUrl(url);

    // if (!isNaN(dispatchUrl.volumeId) && !isNull(dispatchUrl.fid)) {
    //   const fLocation = await this.findLocation(dispatchUrl.volumeId);
    //   return fLocation.publicUrl
    //     .concat('/')
    //     .concat(fLocation.volumeId.toString())
    //     .concat(',')
    //     .concat(dispatchUrl.fid);
    // }

    return this.findLocation(url)
      .then(fLocation => {
        return fLocation.publicUrl.concat('/').concat(url);
      })
      .catch(err => {
        return null;
      });

    return null;
  }

  /**
   * @name delete
   * @param url address of file that we should remove file from file system
   */
  async delete(url: string) {
    if (url === null || url === undefined || url === '') {
      return null;
    }
    const dispatchUrl = await this.dispatchUrl(url);
    if (!isNull(dispatchUrl.fid) && !isNaN(dispatchUrl.volumeId)) {
      return this.findLocation(url)
        .then(fLocation => {
          return Axios.delete(
            `${fLocation.publicUrl}/${fLocation.volumeId},${dispatchUrl.fid}`,
            {
              headers: {
                Authorization: fLocation.auth,
              },
            },
          )
            .then(res => {
              return true;
            })
            .catch(err => {
              return false;
            });
        })
        .catch(err => {
          return false;
        });
    }
  }
  async writeChunk(
    fileData,
    type = '',
    updateUrl: string = null,
  ): Promise<AxiosResponse<WriteChunkResponse>> {
    if (fileData === null) {
      return null;
    }
    const form_data = new FormData();
    let file = null;
    if (isBase64(fileData)) {
      file = Buffer.from(fileData, 'base64');
    } else if (Buffer.isBuffer(fileData)) {
      file = fileData;
    } else {
      return null;
    }

    form_data.append('file', file);
    const formHeaders = form_data.getHeaders();
    const sendUrl: string = this.masterUrl.concat('/submit?pretty=yes');

    if (!isNull(sendUrl)) {
      const uploadFile = Axios.post<WriteChunkResponse>(sendUrl, form_data, {
        headers: {
          ...formHeaders,
        },
      });

      return uploadFile;
    }
    return null;
  }
}
