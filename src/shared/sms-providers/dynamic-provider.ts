import MaxSmsProvider from './max-sms.provider';
import SmsProviderInterface from '../sms.interface';

const Providers = {
  MaxSmsProvider,
};

export default class DynamicProvider implements SmsProviderInterface {
  constructor(ClassName: string, args: any) {
    return new Providers[ClassName](args);
  }
  sendMessage(phoneNumber: string, message: string): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
