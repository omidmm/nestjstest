import SmsProviderInterface from '../sms.interface';
import { json } from 'express';
import Axios from 'axios';
import { Logger } from '@nestjs/common';

export default class MaxSmsProvider implements SmsProviderInterface {
  private config;
  constructor(args: any) {
    this.config = JSON.parse(args);
  }

  async sendMessage(phoneNumber: string, message: string): Promise<boolean> {
    const sms = await Axios.post('http://ippanel.com/api/select', {
      op: 'send',
      uname: this.config.uname,
      pass: this.config.pass,
      message: message,
      from: this.config.from,
      to: phoneNumber,
    });
    return sms.status === 200 ? true : false;
  }
}
