import { Injectable, Logger } from '@nestjs/common';
import Axios from 'axios';
import { ConfigService } from '@nestjs/config';
import SmsProviderInterface from './sms.interface';
import DynamicProvider from './sms-providers/dynamic-provider';
@Injectable()
export class SmsService {
  constructor(private configService: ConfigService) {}

  async send(phoneNumber: string, message: string) {
    const activeSmsProvider: string = this.configService.get(
      'Active_Sms_Provider',
    );

    const smsPrv: SmsProviderInterface = new DynamicProvider(
      activeSmsProvider,
      this.configService.get('Sms_Provider_'.concat(activeSmsProvider)),
    );
    return smsPrv.sendMessage(phoneNumber, message);
  }
}
