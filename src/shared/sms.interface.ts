export default interface SmsProviderInterface {
  sendMessage(phoneNumber: string, message: string): Promise<boolean>;
}
