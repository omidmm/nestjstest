import { createParamDecorator, ExecutionContext } from '@nestjs/common';
const camelcaseKeys = require('camelcase-keys');

export const Metadata = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    return camelcaseKeys(context.getArgByIndex(1));
  },
);
