import { Module } from '@nestjs/common';
import { SeaweedfsService } from './seaweedfs.service';
import { SmsService } from './sms.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule],
  providers: [SeaweedfsService, SmsService],
  exports: [SeaweedfsService, SmsService],
})
export class SharedModule {}
