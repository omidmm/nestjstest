import {
  CanActivate,
  ExecutionContext,
  OnModuleInit,
  Injectable,
  Inject,
} from '@nestjs/common';
import { Client, ClientGrpc, RpcException } from '@nestjs/microservices';
import { AuthClient } from './auth-client';
import * as grpc from 'grpc';
import * as JWTR from 'jwt-redis';

import { ConfigService } from '@nestjs/config';
import { RedisService } from 'nestjs-redis';

@Injectable()
export class GrpcAuthtGuard implements CanActivate, OnModuleInit {
  private authClientService: AuthClient;

  // @Inject('AUTH_PACKAGE')
  // private authClientGrpc: ClientGrpc;
  onModuleInit() {
    // this.authClientService = this.authClientGrpc.getService<AuthClient>(
    //   'AuthClient',
    // );
  }
  constructor(
    private readonly configService: ConfigService,
    private readonly redisService: RedisService,
  ) {}
  getRequest(context: ExecutionContext) {
    return context.switchToRpc().getContext();
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = this.getRequest(context);
    const type = context.getType();
    const prefix = 'Bearer';
    let header: any;
    if (type === 'rpc') {
      const metadata = context.getArgByIndex(1);
      if (!metadata) {
        throw new RpcException({
          code: grpc.status.UNAVAILABLE,
          message: 'metadata unavailable',
        });
      }
      header = metadata.get('Authorization')[0];
    }
    if (!header || !header.includes(prefix)) {
      throw new RpcException({
        code: grpc.status.UNAVAILABLE,
        message: 'Authorization unavailable',
      });
    }
    ///////////////////////////////////////
    if (header.split(' ')[0] !== 'Bearer') {
      throw new RpcException({
        code: grpc.status.UNAUTHENTICATED,
        message: 'Invalid token',
      });
    }

    const jwtr = new JWTR.default(this.redisService.getClient());

    return jwtr
      .verify(
        header.split(' ')[1],
        this.configService.get<string>('JWT_PRIVATE_KEY'),
      )
      .then(response => {
        if (response) {
          request.user = jwtr.decode(header.split(' ')[1]);
          return true;
        } else {
          throw new RpcException({
            code: grpc.status.UNAUTHENTICATED,
            message: 'not found',
          });
        }
      })
      .catch(err => {
        throw new RpcException({
          code: grpc.status.UNAUTHENTICATED,
          message: err.toString(),
        });
      });

    /////////////////////////////////////
    // const token = header;

    // const user = await this.authClientService
    //   .getToken({ token: token })
    //   .toPromise();
    // if (user !== undefined) {
    //   if (user.statusCode === 200) {
    //     request.user = user;
    //     return true;
    //   } else {
    //     throw new RpcException({
    //       code: grpc.status.UNAUTHENTICATED,
    //       message: user.message,
    //     });
    //   }
    // } else {
    //   throw new RpcException({
    //     code: grpc.status.PERMISSION_DENIED,
    //     message: 'No connection Auth service',
    //   });
    // }
  }
}
