import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const GrpcAuthOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: '0.0.0.0:3050',
    package: 'auth',
    protoPath: join(process.cwd(), 'src/shared/auth.proto'),
  },
};
