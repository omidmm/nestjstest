import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { ConfigService } from '@nestjs/config';
import { Logger } from '@nestjs/common';
import * as bodyParser from 'body-parser';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.connectMicroservice({
    transport: Transport.GRPC,
    options: {
      url: configService.get('GRPC_STORE_URL', '0.0.0.0:3041'),
      protoPath: [
        join(process.cwd(), 'src/category/proto/categoryService.proto'),
        join(process.cwd(), 'src/location/proto/location.proto'),
        join(process.cwd(), 'src/store/proto/store.proto'),
        join(process.cwd(), 'src/properties/proto/properties.proto'),
        join(process.cwd(), 'src/brand/proto/brand.proto'),
        join(process.cwd(), 'src/custom-field/proto/cf.proto'),
        join(process.cwd(), 'src/discount/proto/discount.proto'),
        join(process.cwd(), 'src/product/proto/product.proto'),
      ],
      package: [
        'category',
        'location',
        'store',
        'properties',
        'brand',
        'customfield',
        'discount',
        'product',
      ],
    },
  });

  await app.startAllMicroservicesAsync();
  app.use(bodyParser.json({ limit: '500mb' }));
  app.use(bodyParser.urlencoded({ limit: '500mb', extended: true }));
  await app.listen(configService.get('STORE_LISTEN_PORT', 3003));
  Logger.log(
    `Catalog GRPC running on Http://${configService.get(
      'GRPC_STORE_URL',
      3041,
    )}`,
    'Bootstrap',
  );

  Logger.log(
    `Catalog  running on Http://localhost:${configService.get(
      'STORE_LISTEN_PORT',
      3003,
    )}`,
    'Bootstrap',
  );
}
bootstrap();
