import { Controller, UseGuards } from '@nestjs/common';
import { DiscountService } from './discount.service';
import { discount } from './proto/discount';
import { GrpcMethod, Payload } from '@nestjs/microservices';
import * as grpc from 'grpc';
import { GRPCUser } from 'src/shared/user.grpc.decorator';
import { GrpcAuthtGuard } from 'src/shared/grpc-auth.guard';

@Controller()
export class DiscountController {
  constructor(private readonly discountService: DiscountService) {}
  @GrpcMethod('DiscountClient', 'AddDiscount')
  @UseGuards(GrpcAuthtGuard)
  async addDiscount(
    @Payload() data: discount.AddDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.AddDiscountResponse> {
    return this.discountService.addDiscount(data, metadata, user);
  }

  @GrpcMethod('DiscountClient', 'UpdateDiscount')
  @UseGuards(GrpcAuthtGuard)
  async updateDiscount(
    @Payload() data: discount.UpdateDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.UpdateDiscountResponse> {
    return this.discountService.updateDiscount(data, metadata, user);
  }
  @GrpcMethod('DiscountClient', 'DeleteDiscount')
  @UseGuards(GrpcAuthtGuard)
  async deleteDiscount(
    @Payload() data: discount.DeleteDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.DeleteDiscountResponse> {
    return this.discountService.deleteDiscount(data, metadata, user);
  }

  @GrpcMethod('DiscountClient', 'GetDiscountTypes')
  async getDiscountTypes(
    @Payload() data: discount.Null,
    metadata: grpc.Metadata,
  ): Promise<discount.GetDiscountTypesResponse> {
    return this.discountService.getDiscountTypes(data, metadata);
  }

  @GrpcMethod('DiscountClient', 'GetValidIf')
  async getValidIf(
    @Payload() data: discount.Null,
    metadata: grpc.Metadata,
  ): Promise<discount.GetValidIfResponse> {
    return this.discountService.getValidIf(data, metadata);
  }

  @GrpcMethod('DiscountClient', 'GetDiscount')
  @UseGuards(GrpcAuthtGuard)
  async getDiscount(
    @Payload() data: discount.GetDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.GetDiscountResponse> {
    return this.discountService.getDiscount(data, metadata, user);
  }

  @GrpcMethod('DiscountClient', 'FindDiscounts')
  @UseGuards(GrpcAuthtGuard)
  async findDiscounts(
    @Payload() data: discount.FindDiscountsInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.FindDiscountsResponse> {
    return this.discountService.findDiscounts(data, metadata, user);
  }

  @GrpcMethod('DiscountClient', 'UseDiscount')
  @UseGuards(GrpcAuthtGuard)
  async useDiscount(
    @Payload() data: discount.UseDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ): Promise<discount.UseDiscountResponse> {
    return this.discountService.useDiscount(data, metadata, user);
  }

  @GrpcMethod('DiscountClient', 'CheckDiscount')
  @UseGuards(GrpcAuthtGuard)
  async checkDiscount(
    @Payload() data: discount.CheckDiscountInput,
    metadata: grpc.Metadata,
    @GRPCUser() user,
  ) {
    return this.discountService.checkDiscount(data, metadata, user);
  }
}
