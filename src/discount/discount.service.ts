import { Injectable, Inject, OnModuleInit } from '@nestjs/common';
import {
  InjectModel,
  BaseModel,
  timeuuid,
} from '@iaminfinity/express-cassandra';
import { DiscountByStore } from './entities/discount-by-store.entity';
import { DiscountUsageUser } from './entities/discount-usage-user.entity';
import { discount } from './proto/discount';
import { store } from '../store/proto/store';
import * as grpc from 'grpc';
import { ClientGrpc } from '@nestjs/microservices';
import * as _ from 'lodash';
@Injectable()
export class DiscountService implements OnModuleInit {
  async checkDiscount(
    data: discount.CheckDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.CheckDiscountResponse> {
    let disModel;
    if (data.discountCode !== undefined) {
      disModel = this.discountByStoreModel.findOneAsync(
        { store_id: data.storeId, discount_code: data.discountCode },
        { allow_filtering: true },
      );
    } else if (data.discountId != undefined) {
      disModel = this.discountByStoreModel.findOneAsync({
        store_id: data.storeId,
        discount_id: timeuuid(data.discountId),
      });
    } else {
      return {
        exception: {
          message: 'please set discount code or discount id',
          statusCode: grpc.status.NOT_FOUND,
        },
      };
    }
    return disModel.then(discount => {
      if (discount === undefined) {
        return {
          exception: {
            message: 'Sorry, discount not found',
            statusCode: grpc.status.NOT_FOUND,
          },
        };
      }
      if (discount.active === false) {
        return {
          exception: {
            message: 'Sorry, the discount is not active',
            statusCode: grpc.status.INTERNAL,
          },
        };
      }
      const now = new Date().getTime();
      if (discount.valid_from !== null) {
        if (discount.valid_from.getTime() > now) {
          return {
            exception: {
              message: 'Bad news: this discount not started',
              statusCode: grpc.status.INTERNAL,
            },
          };
        }
      }
      if (discount.expire !== null) {
        if (discount.expire.getTime() < now) {
          return {
            exception: {
              message: 'Bad news: the discount expired',
              statusCode: grpc.status.INTERNAL,
            },
          };
        }
      }

      if (discount.max_of_usage <= discount.used) {
        return {
          exception: {
            message: 'Sorry, you can not use the discount, out of usage ',
            statusCode: grpc.status.INTERNAL,
          },
        };
      }
      if (discount.max_usage_of_user !== null) {
        return this.discountUsageUserModel
          .findOneAsync({
            store_id: data.storeId,
            discount_id: timeuuid(discount.discount_id),
            user_id: timeuuid(user.userId),
          })
          .then(disuser => {
            if (disuser === undefined) {
              return {
                exception: {
                  message: 'Success, you can use the discount',
                  statusCode: grpc.status.OK,
                },
              };
            }
            if (disuser.used <= discount.max_usage_of_user) {
              return {
                exception: {
                  message: 'Success, You can use the discount',
                  statusCode: grpc.status.OK,
                },
              };
            } else {
              return {
                exception: {
                  message:
                    'Sorry, you can not use the discount, out of usage user',
                  statusCode: grpc.status.INTERNAL,
                },
              };
            }
          });
      }
      return {
        exception: {
          message: 'Success, you can use the discount',
          statusCode: grpc.status.OK,
        },
      };
    });
  }
  private permission = 'ManageDiscount';
  private storeClient: store.StoreClient;
  private validIf = [
    'Order Total is reached',
    'Discount Code is entered',
    'A specific products os categories  is present in the cart',
  ];
  private discountType = ['Fixed amount', 'Rate'];
  onModuleInit() {
    this.storeClient = this.stClient.getService<store.StoreClient>(
      'StoreClient',
    );
  }
  constructor(
    @Inject('STORE_PACKAGE') private stClient: ClientGrpc,
    @InjectModel(DiscountByStore)
    private discountByStoreModel: BaseModel<DiscountByStore>,
    @InjectModel(DiscountUsageUser)
    private discountUsageUserModel: BaseModel<DiscountUsageUser>,
  ) {}
  async useDiscount(
    data: discount.UseDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.UseDiscountResponse> {
    return this.checkDiscount(
      { storeId: data.storeId, discountId: data.discountId },
      metadata,
      user,
    ).then(checkRes => {
      if (checkRes.exception.statusCode !== grpc.status.OK) {
        return {
          exception: {
            message: checkRes.exception.message,
            statusCode: checkRes.exception.statusCode,
          },
        };
      }

      return this.discountByStoreModel
        .findOneAsync({
          store_id: data.storeId,
          discount_id: timeuuid(data.discountId),
        })
        .then(discount => {
          if (discount === null) {
            return {
              exception: {
                message: 'sorry, discount not found',
                statusCode: grpc.status.NOT_FOUND,
              },
            };
          }
          return this.discountUsageUserModel
            .findOneAsync({
              store_id: data.storeId,
              discount_id: timeuuid(data.discountId),
              user_id: timeuuid(user.userId),
            })
            .then(disuser => {
              if (disuser === undefined) {
                return new this.discountUsageUserModel({
                  store_id: data.storeId,
                  discount_id: timeuuid(data.discountId),
                  orders: [data.orderId],
                  user_id: timeuuid(user.userId),
                })
                  .saveAsync()
                  .then(resSavedUsage => {
                    if (discount.used === null) {
                      discount.used = 1;
                    } else {
                      discount.used = discount.used + 1;
                    }
                    discount.saveAsync();
                    return {
                      exception: {
                        message: 'Success, You used this discount',
                        statusCode: grpc.status.OK,
                      },
                    };
                  })
                  .catch(err => {
                    return {
                      exception: {
                        message: 'Sorry, You can not use the discount Error'.concat(
                          _.toString(err),
                        ),
                        statusCode: grpc.status.INTERNAL,
                      },
                    };
                  });
              } else {
                disuser.used = disuser.used + 1;
                if (_.includes(disuser.orders, data.orderId)) {
                  return {
                    exception: {
                      message:
                        'Sorry,You have already used this discount for this order ',
                      statusCode: grpc.status.INTERNAL,
                    },
                  };
                }
                disuser.orders.push(data.orderId);
              }
              return disuser.saveAsync().then(resSavedUse => {
                if (discount.used === null) {
                  discount.used = 1;
                } else {
                  discount.used = discount.used + 1;
                }
                discount.saveAsync();
                return {
                  exception: {
                    message: 'Success, you used the discount',
                    statusCode: grpc.status.OK,
                  },
                };
              });
            });
        });
    });
    // return this.discountByStoreModel
    //   .findOneAsync({
    //     store_id: data.storeId,
    //     discount_id: timeuuid(data.discountId),
    //   })
    //   .then(discount => {
    //     if (discount === undefined) {
    //       return {
    //         exception: {
    //           message: 'Sorry, Discount not found',
    //           statusCode: grpc.status.NOT_FOUND,
    //         },
    //       };
    //     }
    //     return this.discountUsageUserModel
    //       .findOneAsync({
    //         store_id: data.storeId,
    //         discount_id: timeuuid(data.discountId),
    //         user_id: timeuuid(user.userId),
    //       })
    //       .then(disuser => {
    //         if (disuser !== undefined) {
    //           if (disuser.use <= discount.max_usage_of_user) {
    //             disuser.use = disuser.use + 1;
    //             return disuser.saveAsync().then(val => {
    //               return {
    //                 exception: {
    //                   message: 'Success, You use this discount',
    //                   statusCode: grpc.status.OK,
    //                 },
    //               };
    //             });
    //           }
    //         } else {
    //           return new this.discountUsageUserModel({
    //             discount_id: timeuuid(data.discountId),
    //             store_id: data.storeId,
    //             order_id: timeuuid(data.orderId),
    //             user_id: timeuuid(user.userId),
    //             use: 1,
    //           })
    //             .saveAsync()
    //             .then(val => {
    //               return {
    //                 exception: {
    //                   message: 'Success, You use this discount',
    //                   statusCode: grpc.status.OK,
    //                 },
    //               };
    //             })
    //             .catch(err => {
    //               return {
    //                 exception: {
    //                   message: 'Sorry, There is an Error!'.concat(
    //                     _.toString(err),
    //                   ),
    //                   statusCode: grpc.status.INTERNAL,
    //                 },
    //               };
    //             });
    //         }
    //       });
    //   });
  }
  async findDiscounts(
    data: discount.FindDiscountsInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.FindDiscountsResponse> {
    const checkStaff = await this.checkStaff(
      data.storeId,
      user.userId,
      this.permission,
    );
    if (checkStaff.statusCode !== grpc.status.OK) {
      return {
        exception: {
          message: checkStaff.message,
          statusCode: checkStaff.statusCode,
        },
      };
    }
    const finalRes: discount.FindDiscountsResponse = {
      pageState: null,
      discounts: [],
      exception: {
        message: '',
        statusCode: grpc.status.NOT_FOUND,
      },
    };

    return new Promise((resolve, reject) => {
      this.discountByStoreModel.eachRow(
        { store_id: data.storeId },
        {
          allow_filtering: true,
          pageState: data.pagination.pageState,
          fetchSize: data.pagination.perPage,
        },
        (n, discount) => {
          const fixed_amount: discount.Price[] = [];
          _.each(discount.fixed_amount, function(value, key) {
            fixed_amount.push({
              currencyCode: key,
              amount: value,
            });
          });

          finalRes.discounts.push({
            name: discount.name,
            storeId: discount.store_id,
            discountId: discount.discount_id,
            description: discount.description,
            validFrom:
              discount.valid_from !== null
                ? discount.valid_from.getTime()
                : null,
            expire: discount.expire !== null ? discount.expire.getTime() : null,
            discountType: discount.discount_type,
            validIf: discount.valid_if,
            maxOfUsage: discount.max_of_usage,
            maxUsageOfUser: discount.max_usage_of_user,
            discountCode: discount.discount_code,
            active: discount.active,
            price: fixed_amount,
            categories: discount.categories,
            products: discount.products,
            translation: DiscountService.translationOutput(
              discount.translation,
            ),
            staffId: discount.staff_id,
            rate: discount.rate,
          });
        },
        (err, result) => {
          if (err) {
            finalRes.exception.message = 'Sorry, there is an error! :'.concat(
              _.toString(err),
            );
            reject(err);
          } else {
            finalRes.exception.statusCode = grpc.status.OK;
            (finalRes.exception.message = 'Success, Discounts founded'),
              (finalRes.pageState = result['pageState']);
            return resolve(finalRes);
          }
        },
      );
    });

    // return this.discountByStoreModel
    //   .findAsync(
    //     { store_id: data.storeId },
    //     {
    //       allow_filtering: true,
    //       pageState: data.pagination.pageState,
    //       fetchSize: data.pagination.perPage,
    //     },
    //   )
    //   .then(discounts => {
    //     if (discounts === undefined) {
    //       return {
    //         exception: {
    //           message: 'Sorry, discounts not found',
    //           statusCode: grpc.status.NOT_FOUND,
    //         },
    //       };
    //     }
    //     let discountsRes: discount.Discount[] = [];
    //     discounts.forEach(discount => {
    //       let fixed_amount: discount.Price[] = [];
    //       _.each(discount.fixed_amount, function(value, key) {
    //         fixed_amount.push({
    //           currencyCode: key,
    //           amount: value,
    //         });
    //       });

    //       discountsRes.push({
    //         name: discount.name,
    //         storeId: discount.store_id,
    //         discountId: discount.discount_id,
    //         description: discount.description,
    //         validFrom: (discount.valid_from !== null)? discount.valid_from.getTime(): null,
    //         expire: (discount.expire !== null)? discount.expire.getTime(): null,
    //         discountType: discount.discount_type,
    //         validIf: discount.valid_if  ,
    //         maxOfUsage: discount.max_of_usage,
    //         maxUsageOfUser: discount.max_usage_of_user,
    //         discountCode: discount.discount_code,
    //         active: discount.active,
    //         price: fixed_amount,
    //         categories: discount.categories,
    //         products: discount.products,
    //         translation: DiscountService.translationOutput(discount.translation),
    //         staffId: discount.staff_id,
    //         rate: discount.rate,
    //       });
    //     });
    //     return {
    //       exception: {
    //         message: 'Success, discounts founded',
    //         statusCode: grpc.status.OK,
    //       },
    //       pageState:discounts.pageState,
    //       discounts: discountsRes,
    //     };
    //   })
    //   .catch(err => {
    //     return {
    //       exception: {
    //         message: 'Sorry, There is an Error! : '.concat(_.toString(err)),
    //         statusCode: grpc.status.INTERNAL,
    //       },
    //     };
    //   });
  }
  private async checkStaff(
    storeId: string,
    userId: string,
    role: string = this.permission,
  ): Promise<discount.Exception> {
    const checkStaff = await this.storeClient
      .checkStaff({
        storeId: storeId,
        userId: userId,
        permission: role,
      })
      .toPromise();
    if (checkStaff === undefined) {
      return {
        message: 'Sorry, connection broken!',
        statusCode: grpc.status.NOT_FOUND,
      };
    }
    return {
      message: checkStaff.exception.message,
      statusCode: checkStaff.exception.statusCode,
    };
  }
  async getDiscount(
    data: discount.GetDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.GetDiscountResponse> {
    const checkStaff = await this.checkStaff(
      data.storeId,
      user.userId,
      this.permission,
    );
    if (checkStaff.statusCode !== grpc.status.OK) {
      return {
        exception: {
          message: checkStaff.message,
          statusCode: checkStaff.statusCode,
        },
      };
    }
    return this.discountByStoreModel
      .findOneAsync({
        store_id: data.storeId,
        discount_id: timeuuid(data.discountId),
      })
      .then(discount => {
        if (discount === undefined) {
          return {
            exception: {
              message: 'Sorry, Item not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }
        const fixed_amount: discount.Price[] = [];
        _.each(discount.fixed_amount, function(value, key) {
          fixed_amount.push({
            currencyCode: key,
            amount: value,
          });
        });
        return {
          exception: {
            message: 'Success, Item founded',
            statusCode: grpc.status.OK,
          },
          discount: {
            name: discount.name,
            storeId: discount.store_id,
            discountId: discount.discount_id,
            description: discount.description,
            validFrom:
              discount.valid_from !== null
                ? discount.valid_from.getTime()
                : null,
            expire: discount.expire !== null ? discount.expire.getTime() : null,
            discountType: discount.discount_type,
            validIf: discount.valid_if,
            maxOfUsage: discount.max_of_usage,
            maxUsageOfUser: discount.max_usage_of_user,
            discountCode: discount.discount_code,
            active: discount.active,
            price: fixed_amount,
            categories: discount.categories,
            products: discount.products,
            translation: DiscountService.translationOutput(
              discount.translation,
            ),
            staffId: discount.staff_id,
            rate: discount.rate,
          },
        };
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is an Error: '.concat(_.toString(err)),
            statusCode: grpc.status.INTERNAL,
          },
        };
      });
  }

  async getValidIf(
    data: discount.Null,
    metadata: grpc.Metadata,
  ): Promise<discount.GetValidIfResponse> {
    return {
      types: this.validIf,
      exception: {
        message: 'Success, items founded',
        statusCode: grpc.status.OK,
      },
    };
  }

  async getDiscountTypes(
    data: discount.Null,
    metadata: grpc.Metadata,
  ): Promise<discount.GetDiscountTypesResponse> {
    return {
      types: this.discountType,
      exception: {
        message: 'Success, Items founded',
        statusCode: grpc.status.OK,
      },
    };
  }

  async deleteDiscount(
    data: discount.DeleteDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.DeleteDiscountResponse> {
    const checkStaff = await this.checkStaff(
      data.storeId,
      user.userId,
      this.permission,
    );
    if (checkStaff.statusCode !== grpc.status.OK) {
      return {
        exception: {
          message: checkStaff.message,
          statusCode: checkStaff.statusCode,
        },
      };
    }
    return this.discountByStoreModel
      .findOneAsync({
        store_id: data.storeId,
        discount_id: timeuuid(data.discountId),
      })
      .then(discount => {
        return this.discountUsageUserModel
          .deleteAsync({
            store_id: data.storeId,
            discount_id: timeuuid(data.discountId),
          })
          .then(res => {
            discount.deleteAsync();
            if (res) {
              return {
                exception: {
                  message: 'Success, discount deleted',
                  statusCode: grpc.status.OK,
                },
              };
            }
          })
          .catch(err => {
            return {
              exception: {
                message: 'Sorry, could not delete item '.concat(
                  _.toString(err),
                ),
                statusCode: grpc.status.INTERNAL,
              },
            };
          });
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, Discount could not deleted Error: '.concat(
              _.toString(err),
            ),
            statusCode: grpc.status.INTERNAL,
          },
        };
      });
  }

  async updateDiscount(
    data: discount.UpdateDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.UpdateDiscountResponse> {
    const checkStaff = await this.checkStaff(
      data.discount.storeId,
      user.userId,
      this.permission,
    );
    if (checkStaff.statusCode !== grpc.status.OK) {
      return {
        exception: {
          message: checkStaff.message,
          statusCode: checkStaff.statusCode,
        },
      };
    }
    const fixed_amount = {};
    data.discount.price.forEach(pr => {
      _.set(fixed_amount, pr.currencyCode, pr.amount.toString());
    });
    return this.discountByStoreModel
      .findOneAsync({
        store_id: data.discount.storeId,
        discount_id: timeuuid(data.discount.discountId),
      })
      .then(discount => {
        if (discount === undefined) {
          return {
            exception: {
              message: 'Sorry, Item not found',
              statusCode: grpc.status.NOT_FOUND,
            },
          };
        }
        const now = new Date();
        if (data.discount.validFrom !== undefined) {
          discount.valid_from =
            new Date(parseInt(data.discount.validFrom) * 1000) > now
              ? new Date(parseInt(data.discount.validFrom) * 1000)
              : null;
        }
        if (data.discount.expire !== undefined) {
          discount.expire =
            new Date(parseInt(data.discount.expire) * 1000) > now
              ? new Date(parseInt(data.discount.expire) * 1000)
              : null;
        }
        discount.name = data.discount.name;
        discount.description = data.discount.description;
        discount.valid_if = data.discount.validIf;
        discount.discount_type = data.discount.discountType;
        discount.max_of_usage = data.discount.maxOfUsage;
        discount.max_usage_of_user = data.discount.maxUsageOfUser;
        discount.discount_code = data.discount.discountCode;
        discount.fixed_amount = fixed_amount;
        discount.rate = data.discount.rate;
        discount.categories = data.discount.categories;
        discount.products = data.discount.products;
        discount.translation = DiscountService.translationInput(
          data.discount.translation,
        );
        discount.staff_id = timeuuid(user.userId);
        discount.active = data.discount.active;
        return discount
          .saveAsync()
          .then(value => {
            if (value !== undefined) {
              return {
                exception: {
                  message: 'Success, Discount updated',
                  statusCode: grpc.status.OK,
                },
              };
            }
          })
          .catch(err => {
            return {
              exception: {
                message: 'Sorry, Discount could not saved, cuase of :'.concat(
                  _.toString(err),
                ),
                statusCode: grpc.status.INTERNAL,
              },
            };
          });
      });
  }
  static translationInput(translation: discount.Translation[]) {
    const translateField = {};

    if (translation !== undefined) {
      translation.forEach(trans => {
        const value = {};
        trans.value.forEach(val => {
          value[val.name] = val.value;
        });
        translateField[trans.name] = value;
      });
    }
    return {
      fields: translateField,
    };
  }
  static translationOutput(translations) {
    if (!_.isObject(translations)) {
      return null;
    }
    const result = [];
    _.each(translations.fields, function(value, key) {
      const fieldVal = [];
      _.each(value, function(subval, subkey) {
        fieldVal.push({ name: subkey, value: subval });
      });

      result.push({ name: key, value: fieldVal });
    });
    return result;
  }

  async addDiscount(
    data: discount.AddDiscountInput,
    metadata: grpc.Metadata,
    user: any,
  ): Promise<discount.AddDiscountResponse> {
    const checkStaff = await this.checkStaff(
      data.discount.storeId,
      user.userId,
      this.permission,
    );
    if (checkStaff.statusCode !== grpc.status.OK) {
      return {
        exception: {
          message: checkStaff.message,
          statusCode: checkStaff.statusCode,
        },
      };
    }
    const fixed_amount = {};
    data.discount.price.forEach(pr => {
      _.set(fixed_amount, pr.currencyCode, pr.amount.toString());
    });
    const now = new Date();
    let valid_from, expire: Date;

    if (data.discount.validFrom !== undefined) {
      valid_from =
        new Date(parseInt(data.discount.validFrom) * 1000) > now
          ? new Date(parseInt(data.discount.validFrom) * 1000)
          : null;
    }
    if (data.discount.expire !== undefined) {
      expire =
        new Date(parseInt(data.discount.expire) * 1000) > now
          ? new Date(parseInt(data.discount.expire) * 1000)
          : null;
    }
    return new this.discountByStoreModel({
      name: data.discount.name,
      store_id: data.discount.storeId,
      discount_id: timeuuid(),
      discount_type: data.discount.discountType,
      valid_from: valid_from,
      expire: expire,
      description: data.discount.description,
      valid_if: data.discount.validIf,
      active: data.discount.active,
      max_of_usage: data.discount.maxOfUsage,
      max_usage_of_user: data.discount.maxUsageOfUser,
      fixed_amount: fixed_amount,
      categories: data.discount.categories,
      products: data.discount.products,
      staff_id: timeuuid(user.userId),
      rate: data.discount.rate,
      translation: DiscountService.translationInput(data.discount.translation),
      discount_code: data.discount.discountCode,
    })
      .saveAsync()
      .then(item => {
        if (item !== undefined) {
          return {
            exception: {
              message: 'Success, Discount saved',
              statusCode: grpc.status.OK,
            },
          };
        }
      })
      .catch(err => {
        return {
          exception: {
            message: 'Sorry, there is an Error'.concat(err),
            statusCode: grpc.status.INTERNAL,
          },
        };
      });
  }
}
