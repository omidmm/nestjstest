import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'discount_usage_user',
  key: [['store_id', 'discount_id'], 'user_id'],
  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
})
export class DiscountUsageUser {
  @Column({
    type: 'varchar',
  })
  store_id: string;
  @Column({
    type: 'timeuuid',
  })
  discount_id: any;
  @Column({
    type: 'timeuuid',
  })
  user_id: any;

  @Column({
    type: 'set',
    typeDef: '<text>',
  })
  orders: any;
  @Column({
    type: 'int',
    default: () => {
      return 1;
    },
  })
  used: number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}
