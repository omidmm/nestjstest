import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from '@iaminfinity/express-cassandra';

@Entity({
  table_name: 'discount_by_store',
  key: ['store_id', 'discount_id', 'discount_code'],
  options: {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },
  },
})
export class DiscountByStore {
  @Column({
    type: 'varchar',
  })
  store_id: any;
  @Column({
    type: 'timeuuid',
  })
  discount_id: any;
  @Column({
    type: 'varchar',
  })
  name: string;
  @Column({
    type: 'text',
  })
  description: string;
  @Column({
    type: 'timestamp',
  })
  valid_from: Date;
  @Column({
    type: 'timestamp',
  })
  expire: Date;

  @Column({
    type: 'varchar',
  })
  discount_type: string;
  @Column({
    type: 'varchar',
  })
  valid_if: string;

  @Column({
    type: 'int',
  })
  max_of_usage: number;
  @Column({
    type: 'int',
  })
  max_usage_of_user: number;
  @Column({
    type: 'varchar',
  })
  discount_code: string;
  @Column({
    type: 'boolean',
    default: () => {
      return true;
    },
  })
  active: boolean;
  @Column({
    type: 'map',
    typeDef: '<varchar,varchar>',
  })
  fixed_amount: object;

  @Column({
    type: 'int',
  })
  rate: number;

  @Column({
    type: 'set',
    typeDef: '<text>',
  })
  categories: string;

  @Column({
    type: 'set',
    typeDef: '<text>',
  })
  products: string;

  @Column({
    type: 'frozen',
    typeDef: '<translation>',
  })
  translation: any;

  @Column({
    type: 'timeuuid',
  })
  staff_id: any;
  @Column({
    type: 'int',
  })
  used: number;
  @CreateDateColumn()
  created_at: Date;
  @UpdateDateColumn()
  updated_at: Date;
}
