import { Module } from '@nestjs/common';
import { ExpressCassandraModule } from '@iaminfinity/express-cassandra';
import { SharedModule } from 'src/shared/shared.module';
import { DiscountService } from './discount.service';
import { DiscountController } from './discount.controller';
import { DiscountByStore } from './entities/discount-by-store.entity';
import { DiscountUsageUser } from './entities/discount-usage-user.entity';
import { Transport, ClientsModule } from '@nestjs/microservices';
import { join } from 'path';

@Module({
  imports: [
    ExpressCassandraModule.forFeature([DiscountByStore, DiscountUsageUser]),
    SharedModule,
    ClientsModule.register([
      {
        name: 'AUTH_PACKAGE',
        transport: Transport.GRPC,

        options: {
          url: '0.0.0.0:3050',
          package: 'auth',
          protoPath: join(process.cwd(), 'src/shared/auth.proto'),
        },
      },
      {
        name: 'STORE_PACKAGE',
        transport: Transport.GRPC,
        options: {
          url: '0.0.0.0:3041',
          package: 'store',
          protoPath: join(process.cwd(), 'src/store/proto/store.proto'),
        },
      },
    ]),
  ],
  controllers: [DiscountController],
  providers: [DiscountService],
})
export class DiscountModule {}
