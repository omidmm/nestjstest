export abstract class MockModel<T> {
  protected abstract entityStub: T;
  constructor(createEntityData: T) {
    this.constructorSpy(createEntityData);
  }
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructorSpy(createEntityData: T): void {}
  findOneAsync(): { exec: () => T } {
    return {
      exec: (): T => this.entityStub,
    };
  }
  search(): { exec: () => T[] } {
    return {
      exec: (): T[] => [this.entityStub],
    };
  }
}
