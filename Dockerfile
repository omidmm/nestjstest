FROM node:14.5-alpine as development

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -g @nestjs/cli
RUN npm install 

COPY . .


FROM node:14.5-alpine as production

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install --only=production
COPY . .
COPY --from=development /usr/src/app/dist ./dist
CMD ["node","dist/main"]